This file contains installation instructions to the Trouble Ticketing System:

0. Ticketing runs under the following license
  GNU GPL v2.0

1. Software components pre-required to install Trouble Ticketing System:
  - php-recode
  - Drupal 6.x (http://drupal.org/project/Drupal+project)
  - sqlalchemy 0.3.0 or newer (possibly http://www.sqlalchemy.org/download.myt)

 Please check out how to install these pieces of software from their
 installation manuals!

2. Unwrap the package

   $ tar -xvvzf ticketing-6.x-*.tar.gz
   $ cd ticketing

 Then copy the Ticketing module to the Drupal modules folder. If Drupal is
 installed under /var/www/drupal, then:

   $ cp -r ticketing/  /var/www/drupal/sites/all/modules/ticket

3. Installation

 3.1. Activation of the Ticketing Module

  Enable the Ticketing Module in the Drupal. This can be done as follows:
  If the Drupal is installed on http://localhost/drupal, then visit page
  http://localhost/drupal/?q=admin/modules, choose the Ticketing Module
  from the list and save settings.

  Then visit the following page: http://localhost/drupal/?q=admin/ticket !

  Create Organizational Units: http://localhost/drupal/?q=admin/ticket/add/unit !

  Then the first Group: http://localhost/drupal/?q=admin/ticket/group/add/1 !

  Adjust group settings:  http://localhost/drupal/?q=/admin/ticket/group/edit/1 !

  The module credentials can be set at:
    http://localhost/drupal/?q=admin/ticket/access.

  If all of these are set, then create trouble tickets:
    http://localhost/drupal/?q=ticket.

  Tickets can be referred directly as: http://localhost/drupal/?q=ticketid/[NUM] 

 3.2. Setting up the email gateway

  Copy the gateway to any location:
    $ cp ticketing/e-mail/mail2ticket.py /usr/local/bin/

  Then add a line similar to the following into the /etc/aliases file.
    email: path/mail2ticket.py email_address

  The first email address is where the mail arrives at. The second address is
  the same as set in "Group e-mail address" in the Drupal.
  Example:
    ticket: "|/usr/local/bin/mail2ticket.py ticket@example.org"

  Then do not forget to issue command newaliases.

  Note that the mail gateway and the Drupal module should access to the same
  database to work properly! So set the same database credentials in ticket.py 
  as you have in Drupal.

4. Remove unnecessary files

  Remove the unnecessary files from Drupal's module directory:
   $ rm -r /var/www/drupal/sites/all/modules/ticket/doc
   $ rm -r /var/www/drupal/sites/all/modules/ticket/e-mail
   $ rm -r /var/www/drupal/sites/all/modules/ticket/utils
