<?php
/*
 * Created on 2006.04.29.
 */
include_once drupal_get_path('module', 'ticket') . '/extra_form_select.inc';
include_once drupal_get_path('module', 'ticket') . '/conf.inc';
// PHP4 Only!!!
include_once drupal_get_path('module', 'ticket') . '/ticket4.inc';
/**
 * Display help and module information
 * @param section which section of the site we're displaying help
 * @return help text for section
 */
function ticket_help($section = '') {
	$output = '';

	switch ($section) {
		case "admin/modules#description":
			$output = t('Ticketing module');
		break;

		case "admin/help#ticket":
			$output = t("By using this module the authorized can open trouble tickets, add events to them, and when the trouble has terminated can close the ticket.<br />\r\nTickets may belong to organizational units and can be classified into groups within units.<br />\r\nThe module has sophisticated search and export possibilities.<br />\r\nTo use this module a Unit and at least one group should be created.");
		break;
		case 'admin/ticket':
			$output = t('In this page the trouble ticketing system module can be configured.');
		break;
	}
	return $output;
} // function ticket_help()

/**
 * Valid permissions for this module
 * @return array An array of valid permissions for the ticket module
 */
function ticket_perm() {
	return array('ticket reader', 'ticket manager', 'ticket outsider', 'ticket administer');
} // function ticket_perm()

/**
 * Determine whether the current user may perform the given operation on the
 * specified ticket.
 *
 * @param $perm
 *   ticket permissions
 * @param $u_id
 *    Unit id or '*'.
 * @param $gid
 *    Group id or '*'.
 * @param $uid
 *   The user ID on which the operation is to be performed.
 * @return
 *   TRUE if the operation may be performed.
 */
function ticket_access($perm, $u_id = 0, $gid = 0, $uid = NULL) {
	global $user;

	if ($user->uid == 1) {
		return TRUE;
	}

	/** A jogosultsaggal rendelkezo csoportok listaja */
	$result = db_query("SELECT rid FROM {permission} WHERE perm LIKE '%%%s%' ORDER BY rid", $perm);
	$roles = array();
	while ($row = db_fetch_array($result)) {
		$roles[$row['rid']] = $row['rid'];
	}
	/** A felhasznalo csoportjai es a jogosultsagot tartalmazo csoportok metszete. */
	if ( count($roles) && count($user->roles) ) {
		$access = array_intersect_key($user->roles, $roles);
	}
	/** Ha van kozos csoport */
	if ( count($access) ) {
		if ((string)$u_id != '*') {
			$sql['u_id'] = '(tp.u_id = %d OR tp.u_id = 0) AND';
		} else {
			$sql['u_id'] = '(%d) AND ';
			$u_id = 1;
		}

		if ((string)$gid != '*') {
			$sql['gid'] = '(tp.gid = %d OR tp.gid = 0) AND';
		} else {
			$sql['gid'] = '(%d) AND ';
			$gid = 1;
		}

		/** Ticket jogosultsag ellenorzese */
		$SQL = "SELECT tp.role_id FROM {ticket_permission} tp, {role} r WHERE r.rid = tp.role_id AND ".$sql['u_id']." ".$sql['gid']." tp.role_id IN (%s) ORDER BY tp.role_id";
		$result = db_query($SQL, $u_id, $gid, implode(',', array_keys($access)));
		if ( db_num_rows($result) ) {
			return TRUE;
		}
	}
	return FALSE;
}

/**
 * ticket_menu: menus
 */
function ticket_menu($may_cache) {
	$items = array();

	if ($may_cache) {
		$items[] = array('path' => 'ticket',
			'title' => t('Tickets'),
			'callback' => 'ticket_info',
			'access' => ticket_access('ticket reader', '*', '*'),
		);

		$items[] = array('path' => 'ticketid',
			'title' => 'Jump ticket ID',
			'callback' => 'ticket_ticketid',
			'access' => ticket_access('ticket reader', '*', '*'),
			'type' => MENU_CALLBACK,
		);

		// Global Search
		$items[] = array('path' => 'ticket/search',
			'title' => t('Search'),
			'callback' => 'ticket_search_simple',
			'callback arguments' => array(NULL, NULL),
			'access' => ticket_access('ticket manager', '*', '*'),
			'weight' => 8,
		);

		$items[] = array('path' => 'ticket/search/tid',
			'title' => t('Ticket ID search'),
			'callback' => 'ticket_search_tid',
			'callback arguments' => array(NULL, NULL),
			'access' => ticket_access('ticket manager', '*', '*'),
			'weight' => -2,
			'type' => MENU_LOCAL_TASK,
		);

		$items[] = array('path' => 'ticket/search/simple',
			'title' => t('Search'),
			'callback arguments' => array(NULL, NULL),
			'access' => ticket_access('ticket manager', '*', '*'),
			'weight' => 0,
			'type' => MENU_DEFAULT_LOCAL_TASK,
		);

		$items[] = array('path' => 'ticket/search/advanced',
			'title' => t('Advanced search'),
			'callback' => 'ticket_search_advanced',
			'callback arguments' => array(NULL, NULL),
			'access' => ticket_access('ticket manager', '*', '*'),
			'weight' => 2,
			'type' => MENU_LOCAL_TASK,
		);

		/* Admin menuk */
		if (user_access('access administration pages')) {
			$title = t('ticket');
		} elseif (user_access('ticket administer')) {
			$title = t('Ticketing settings');
		}

		$items[] = array('path' => 'admin/ticket',
			'title' => $title,
			'callback' => 'ticket_admin',
			'access' => user_access('ticket administer'),
		);

		$items[] = array('path' => 'admin/ticket/list',
			'title' => t('List'),
			'access' => user_access('ticket administer'),
			'type' => MENU_DEFAULT_LOCAL_TASK,
			'weight' => -10,
		);

		$items[] = array('path' => 'admin/ticket/add/unit',
			'title' => t('Add unit'),
			'callback' => 'ticket_admin_add_unit',
			'access' => ticket_access('ticket administer', 0, '*'),
			'weight' => 2,
			'type' => MENU_LOCAL_TASK,
		);

		$items[] = array('path' => 'admin/ticket/access',
			'title' => t('Access'),
			'callback' => 'ticket_admin_access',
			'access' => ticket_access('ticket administer', '*', '*'),
			'weight' => 0,
			'type' => MENU_LOCAL_TASK,
		);

		$items[] = array('path' => 'admin/ticket/access/list',
			'title' => t('List access'),
			'access' => ticket_access('ticket administer', '*', '*'),
			'type' => MENU_DEFAULT_LOCAL_TASK,
			'weight' => -10,
		);

		$items[] = array('path' => 'admin/ticket/access/add',
			'title' => t('Add access'),
			'callback' => 'ticket_admin_access_add_edit',
			'access' => user_access('ticket administer'),
			'type' => MENU_LOCAL_TASK,
		);

		$items[] = array('path' => 'admin/ticket/access/users',
			'title' => t('Users'),
			'callback' => 'ticket_admin_access_users',
			'access' => user_access('ticket administer'),
			'type' => MENU_LOCAL_TASK,
		);

		$items[] = array('path' => 'admin/ticket/access/edit',
			'title' => t('Edit access'),
			'callback' => 'ticket_admin_access_add_edit',
			'access' => user_access('ticket administer'),
			'type' => MENU_CALLBACK,
		);

		$items[] = array('path' => 'admin/ticket/access/delete',
			'title' => t('Deleted access'),
			'callback' => 'ticket_admin_access_delete_confirm',
			'access' => user_access('ticket administer'),
			'type' => MENU_CALLBACK,
		);

	} else {
		/**
		 * Ticket readers menus
		 */

		$ticket_menus = _ticket_unit_group_list('ticket reader', '/', -1);
		if ( count($ticket_menus) ) {
			foreach ($ticket_menus as $key => $ticket_menu) {
				list($u_id, $gid) = explode('/', $key);
				if (isset($gid)) {
					$items[] = array('path' => 'ticket/'.$key,
						'title' => $ticket_menu,
						'callback' => 'ticket_simple_list',
						'callback arguments' => array($u_id, $gid),
						'access' => ticket_access('ticket reader', $u_id, $gid),
					);

					$items[] = array('path' => 'ticket/'.$key.'/search',
						'title' => t('Search'),
						'callback' => 'ticket_search_simple',
						'callback arguments' => array($u_id, $gid),
						'access' => ticket_access('ticket reader', $u_id, $gid),
						'weight' => 8,
					);

					$items[] = array('path' => 'ticket/'.$key.'/search/tid',
						'title' => t('Ticket ID search'),
						'callback' => 'ticket_search_tid',
						'access' => ticket_access('ticket reader', $u_id, $gid),
						'weight' => -2,
						'type' => MENU_LOCAL_TASK,
					);

					$items[] = array('path' => 'ticket/'.$key.'/search/simple',
						'title' => t('Search'),
						'callback arguments' => array($u_id, $gid),
						'access' => ticket_access('ticket reader', $u_id, $gid),
						'weight' => 0,
						'type' => MENU_DEFAULT_LOCAL_TASK,
					);

				} else {
					$items[] = array('path' => 'ticket/'.$key,
						'title' => $ticket_menu,
						'callback' => 'ticket_info',
						'callback arguments' => array($u_id),
						'access' => ticket_access('ticket reader', $u_id, '*'),
					);

					$items[] = array('path' => 'ticket/'.$key.'/search',
						'title' => t('Search'),
						'callback' => 'ticket_search_simple',
						'callback arguments' => array($u_id, NULL),
						'access' => ticket_access('ticket reader', $u_id, '*'),
						'weight' => 8,
					);

					$items[] = array('path' => 'ticket/'.$key.'/search/tid',
						'title' => t('Ticket ID search'),
						'callback' => 'ticket_search_tid',
						'callback arguments' => array($u_id, NULL),
						'access' => ticket_access('ticket reader', $u_id, '*'),
						'weight' => -2,
						'type' => MENU_LOCAL_TASK,
					);

					$items[] = array('path' => 'ticket/'.$key.'/search/simple',
						'title' => t('Search'),
						'callback arguments' => array($u_id, NULL),
						'access' => ticket_access('ticket reader', $u_id, '*'),
						'weight' => 0,
						'type' => MENU_DEFAULT_LOCAL_TASK,
					);
				}
			}
		}

		/**
		 * Ticket outsider menus
		 */
		$ticket_menus = _ticket_unit_group_list('ticket outsider', '/', -1);
		if ( count($ticket_menus) ) {
			foreach ($ticket_menus as $key => $ticket_menu) {
				list($u_id, $gid) = explode('/', $key);
				if (isset($gid)) {
					$items[] = array('path' => 'ticket/'.$key,
						'title' => $ticket_menu,
						'callback' => 'ticket_simple_list',
						'callback arguments' => array($u_id, $gid),
						'access' => ticket_access('ticket outsider', $u_id, $gid),
					);
					$items[] = array('path' => 'ticket/'.$key.'/add',
						'title' => t('Add ticket'),
						'callback' => 'ticket_add_queue',
						'callback arguments' => array($u_id, $gid),
						'access' => ticket_access('ticket outsider', $u_id, $gid),
					);
				} else {
					$items[] = array('path' => 'ticket/'.$key,
						'title' => $ticket_menu,
						'callback' => 'ticket_info',
						'callback arguments' => array($u_id),
						'access' => ticket_access('ticket outsider', $u_id, '*'),
					);
				}
			}
		}

		/**
		 * Ticket manager menus
		 */
		$ticket_menus = _ticket_unit_group_list('ticket manager', '/', -1);
		if ( count($ticket_menus) ) {
			foreach ($ticket_menus as $key => $ticket_menu) {
				list($u_id, $gid) = explode('/', $key);
				if (isset($gid)) {
					$items[] = array('path' => 'ticket/'.$key,
						'title' => $ticket_menu,
						'callback' => 'ticket_simple_list',
						'callback arguments' => array($u_id, $gid),
						'access' => ticket_access('ticket manager', $u_id, $gid),
					);
					$items[] = array('path' => 'ticket/'.$key.'/add',
						'title' => t('Add ticket'),
						'callback' => 'ticket_add',
						'callback arguments' => array($u_id, $gid),
						'access' => ticket_access('ticket manager', $u_id, $gid),
					);
					$items[] = array('path' => 'ticket/'.$key.'/list',
						'title' => t('List'),
						'callback' => 'ticket_list',
						'callback arguments' => array($u_id, $gid),
						'access' => ticket_access('ticket manager', $u_id, $gid),
						'weight' => 6,
					);
					$items[] = array('path' => 'ticket/'.$key.'/search',
						'title' => t('Search'),
						'callback' => 'ticket_search_simple',
						'callback arguments' => array($u_id, $gid),
						'access' => ticket_access('ticket manager', $u_id, $gid),
						'weight' => 8,
					);

					$items[] = array('path' => 'ticket/'.$key.'/search/tid',
						'title' => t('Ticket ID search'),
						'callback' => 'ticket_search_tid',
						'access' => ticket_access('ticket manager', $u_id, $gid),
						'weight' => -2,
						'type' => MENU_LOCAL_TASK,
					);

					$items[] = array('path' => 'ticket/'.$key.'/search/simple',
						'title' => t('Search'),
						'callback arguments' => array($u_id, $gid),
						'access' => ticket_access('ticket manager', $u_id, $gid),
						'weight' => 0,
						'type' => MENU_DEFAULT_LOCAL_TASK,
					);

					$items[] = array('path' => 'ticket/'.$key.'/search/advanced',
						'title' => t('Advanced search'),
						'callback' => 'ticket_search_advanced',
						'callback arguments' => array($u_id, $gid),
						'access' => ticket_access('ticket manager', $u_id, $gid),
						'weight' => 2,
						'type' => MENU_LOCAL_TASK,
					);

					$items[] = array('path' => 'ticket/'.$key.'/export',
						'title' => t('Export'),
						'callback' => 'ticket_export',
						'callback arguments' => array($u_id, $gid),
						'access' => ticket_access('ticket manager', $u_id, $gid),
						'weight' => 10,
					);
				} else {
					$items[] = array('path' => 'ticket/'.$key,
						'title' => $ticket_menu,
						'callback' => 'ticket_info',
						'callback arguments' => array($u_id),
						'access' => ticket_access('ticket manager', $u_id, '*'),
					);
					$items[] = array('path' => 'ticket/'.$key.'/list',
						'title' => t('List'),
						'callback' => 'ticket_list',
						'callback arguments' => array($u_id, $gid),
						'access' => ticket_access('ticket manager', $u_id, '*'),
						'weight' => 6,
					);
					$items[] = array('path' => 'ticket/'.$key.'/search',
						'title' => t('Search'),
						'callback' => 'ticket_search_simple',
						'callback arguments' => array($u_id, NULL),
						'access' => ticket_access('ticket manager', $u_id, '*'),
						'weight' => 8,
					);

					$items[] = array('path' => 'ticket/'.$key.'/search/tid',
						'title' => t('Ticket ID search'),
						'callback' => 'ticket_search_tid',
						'callback arguments' => array($u_id, NULL),
						'access' => ticket_access('ticket manager', $u_id, '*'),
						'weight' => -2,
						'type' => MENU_LOCAL_TASK,
					);

					$items[] = array('path' => 'ticket/'.$key.'/search/simple',
						'title' => t('Search'),
						'callback arguments' => array($u_id, NULL),
						'access' => ticket_access('ticket manager', $u_id, '*'),
						'weight' => 0,
						'type' => MENU_DEFAULT_LOCAL_TASK,
					);

					$items[] = array('path' => 'ticket/'.$key.'/search/advanced',
						'title' => t('Advanced search'),
						'callback' => 'ticket_search_advanced',
						'callback arguments' => array($u_id, NULL),
						'access' => ticket_access('ticket manager', $u_id, '*'),
						'weight' => 2,
						'type' => MENU_LOCAL_TASK,
					);

					$items[] = array('path' => 'ticket/'.$key.'/export',
						'title' => t('Export'),
						'callback' => 'ticket_export',
						'callback arguments' => array($u_id, NULL),
						'access' => ticket_access('ticket manager', $u_id, '*'),
						'weight' => 10,
					);
				}
			}
		}

		if ( (arg(0) == 'ticket') && ( is_numeric(arg(1)) ) && ( is_numeric(arg(2)) ) ) {
			$result = db_query("SELECT count(*) AS count FROM {ticket_queue} WHERE gid = %d GROUP BY gid", arg(2));
			if ( $row = db_fetch_array($result) ) {
				$items[] = array('path' => 'ticket/'.arg(1).'/'.arg(2).'/queue',
					'title' => t('Queue (%num)', array('%num' => $row['count']) ),
					'callback' => 'ticket_queue',
					'callback arguments' => array(arg(1), arg(2)),
					'access' => ticket_access('ticket manager', arg(1), arg(2)),
				);
			}
		}

		if ( (arg(0) == 'ticket') && ( is_numeric(arg(1)) ) && ( is_numeric(arg(2)) ) && ( is_numeric(arg(3)) ) ) {
			$t_access = 'ticket reader';
			if (ticket_access('ticket manager', arg(1), arg(2))) {
				$t_access = 'ticket manager';
			}

			$items[] = array('path' => 'ticket/'.arg(1).'/'.arg(2).'/'.arg(3),
				'title' => t('Ticket'),
				'callback' => 'ticket_ticket',
				'callback arguments' => array(arg(1), arg(2), arg(3)),
				'access' => ticket_access($t_access, arg(1), arg(2)),
			);
		}

		/**
		 * Administrator menus
		 */
		if ( (arg(0) == 'admin') && (arg(1) == 'ticket') ) {
			if ( (is_numeric(arg(2))) && ( arg(2) > 0 ) ) {
				$items[] = array('path' => 'admin/ticket/'.arg(2),
					'title' => t('Groups'),
					'callback' => 'ticket_admin_list_groups',
					'callback arguments' => array(arg(2)),
					'access' => ticket_access('ticket administer', arg(2), '*'),
					'type' => MENU_CALLBACK,
				);

				$items[] = array('path' => 'admin/ticket/'.arg(2).'/list',
					'title' => t('List groups'),
					'access' => ticket_access('ticket administer', arg(2), '*'),
					'type' => MENU_DEFAULT_LOCAL_TASK,
					'weight' => -10,
				);

				$items[] = array('path' => 'admin/ticket/'.arg(2).'/add/group',
					'title' => t('Add group'),
					'callback' => 'ticket_admin_add_group',
					'callback arguments' => array(arg(2)),
					'access' => ticket_access('ticket administer', arg(2), 0),
					'type' => MENU_LOCAL_TASK,
				);
			}

			if ( (is_numeric(arg(4))) && ( arg(4) > 0 ) ) {
				$items[] = array('path' => 'admin/ticket/edit/unit/'. arg(4),
					'title' => t('Edit unit'),
					'callback' => 'ticket_admin_edit_unit',
					'callback arguments' => array(arg(4)),
					'access' => ticket_access('ticket administer', arg(4), 0),
					'type' => MENU_CALLBACK,
				);

				$items[] = array('path' => 'admin/ticket/edit/group',
					'title' => t('Edit group'),
					'callback' => 'ticket_admin_edit_group',
					'access' => ticket_access('ticket administer', '*', arg(4)),
					'access' => 1,
					'type' => MENU_CALLBACK,
				);

				$items[] = array('path' => 'admin/ticket/edit/template',
					'title' => t('E-mail template'),
					'callback' => 'ticket_admin_edit_template',
					'access' => ticket_access('ticket administer', '*', arg(4)),
					'access' => 1,
					'type' => MENU_CALLBACK,
				);

				$items[] = array('path' => 'admin/ticket/set',
					'title' => t('Set fixer'),
					'callback' => 'ticket_admin_set',
					'access' => ticket_access('ticket administer', '*', arg(4)),
					'type' => MENU_CALLBACK,
				);
			}
		}

		// HELP !!!
		$items[] = array('path' => 'ticket/help',
			'title' => t('help'),
			'callback' => 'ticket_user_help',
			'access' => 1,
			'type' => MENU_CALLBACK,
		);

		$items[] = array('path' => 'admin/ticket/edit/help',
			'title' => t('help'),
			'callback' => 'ticket_admin_help',
			'access' => user_access('ticket administer'),
			'type' => MENU_CALLBACK,
		);
	}
	return $items;
} // function ticket_menu()

/**
 * ticket_cron - idozitett munkafolyamatok 
 */
function ticket_cron() {
	$update = variable_get('ticket_unchanged', 0);
	$now = time();
	if ( ($now - $update) >= 86400) {
		$unc = array();
		variable_set('ticket_unchanged', strtotime(date('Y-m-d 06:00', $now)));
		$SQL = 'SELECT tt.gid, tt.unchanged_day, tt.mailto, tt.usubject, tt.utemplate ';
		$SQL .= " FROM {ticket_template} tt, {ticket_group} tg ";
		$SQL .= " WHERE tt.unchanged_day > 0 AND tg.active = 1 AND tt.gid = tg.gid";
		$result = db_query($SQL);
		while ($row = db_fetch_array($result)) {
			$uday = $now-($now % 86400) - ($row['unchanged_day']*86400);
			$SQL = "SELECT t.tid, MAX(te.created) AS date FROM {ticket_ticket} t, {ticket_event} te";
			$SQL .= " WHERE t.gid = %d AND t.created < %d AND t.closed = 0 AND t.tid = te.tid GROUP BY t.tid HAVING MAX(te.created) < %d";
			$rs = db_query($SQL, $row['gid'], $uday, $uday);
			$tids = $wheres = array();
			while ($sub_row = db_fetch_array($rs)) {
				$wheres[] = '(t.tid = '.$sub_row['tid']." AND te.created = '".$sub_row['date']."')";
			}
			$mail = $mailto = array();
			$mail['mailfrom'] = $row['mailto'];
			$mail['subject'] = $row['usubject'];
			$mail['utemplate'] = $row['utemplate'];
			$rs = db_query('SELECT mail_name, mailto FROM {ticket_mail} WHERE gid = %d AND def = 1', $row['gid']);
			
			while ($sub_row = db_fetch_array($rs)) {
				$mailto[] = $sub_row['mail_name'].' <'.$sub_row['mailto'].'>';
			}
			if ( ($mail['mailfrom'] != '') && ($mail['subject'] != '') && ($mail['utemplate'] != '') && count($mailto) ) {
				$mail['msg'] = '';
				if ( count($wheres) ) {
					$chunk = array_chunk($wheres, 30);
					foreach ($chunk as $where) {
						$SQL = "SELECT t.tid, tg.group_name, t.uid, tp.priority_name, tt.type_name, ts.status_name, tsc.scope_name";
						$SQL .= ", tsi.site_name, tf.fixer_name, t.assigned, t.start, te.created, t.summary";
						$SQL .= " FROM {ticket_ticket} t, {ticket_group} tg, {ticket_event} te, {ticket_status} ts, {ticket_priority} tp, {ticket_type} tt";
						$SQL .= ", {ticket_scope} tsc, {ticket_site} tsi, {ticket_fixer} tf";
						$SQL .= " WHERE t.gid = tg.gid AND t.tid = te.tid AND te.status_id = ts.status_id AND t.priority_id = tp.priority_id";
						$SQL .= " AND t.type_id = tt.type_id AND t.scope_id = tsc.scope_id";
						$SQL .= " AND t.site_id = tsi.site_id AND t.fixer_id = tf.fixer_id";
						$SQL .= " AND (".implode(' OR ', $where).") ORDER BY ts.status_name, te.created, t.tid";
						$w_rs = db_query($SQL);
						while ($w_row = db_fetch_array($w_rs)) {
							$var = array();
							$var['%tid'] = $w_row['tid'];
							$var['%g'] = $w_row['group_name'];
							$var['%pri'] = $w_row['priority_name'];
							$var['%type'] = $w_row['type_name'];
							$var['%st'] = $w_row['status_name'];
							$var['%sc'] = $w_row['scope_name'];
							$var['%si'] = $w_row['site_name'];
							$var['%fixer'] = $w_row['fixer_name'];
							$var['%ps'] = format_date($w_row['start'], 'small');
							$var['%lm'] = format_date($w_row['created'], 'small');
							$var['%ss'] = $w_row['summary'];
	
							$var['%pl'] = format_interval(time() - $w_row['start'], 3);
		
							$var['%tc'] = _ticket_get_user($w_row['uid']);
							$var['%assigned'] = _ticket_get_user($w_row['assigned']);
							
							$mail['msg'] .= strtr($mail['utemplate'], $var)."\n";
						}
					}
				}			
				$mail['header'] = "From: ".$mail['mailfrom']."\nReply-to: ".$mail['mailfrom']."\nX-Mailer: Drupal";
				$mail['msg'] = wordwrap($mail['msg'], 70);
				$mail['msg'] = str_replace("\n.", "\n..", $mail['msg']);
				if ($mail['msg'] != '') {
					user_mail(implode(', ', $mailto), $mail['subject'], $mail['msg'], $mail['header']);
				}
			}
		}
	}
}

/**
 * _ticket_breadcrumb: list groups breadcrumb kiiratasa
 * @param: $link - teljes link
 * @param: $name - linkekhez tartozo megnevezes 
 */
function _ticket_breadcrumb($link, $name = array()) {
	$links = explode('/', $link);
	$base = '';
	foreach ($links as $link) {
		$base .= $link;
		if ( isset($name[$base]) ) {
			$breadcrumb[] = array('path' => $base, 'title' => $name[$base]);
		} else {
			$breadcrumb[] = array('path' => $base);
		}
		$base .= '/';
	}
	$breadcrumb[] = array('path' => $_GET['q']);
	menu_set_location($breadcrumb);
}

/**
 * _ticket_get_user: uid-bol vissza adja a felhasznalo nevet
 * @param: $uid - drupal user id
 */
function _ticket_get_user($uid) {
	if ($uid) {
		$result = db_query("SELECT name FROM {users} WHERE uid = %d", $uid);
		$row = db_fetch_array($result);
		$name = $row['name'];
	} else {
		$name = variable_get('anonymous', 'Anonymous');
	}

	return $name;
}


/**
 * _ticket_secondary_menu: masodlagos menu sajat kezzel.
 * @param: $menus - menuk
 * @param: $active - kivalasztott
 */
function _ticket_secondary_menu($menus, $active = '') {
	$output = '';
	$output .= '<div class="tabs"><ul class="tabs secondary">'."\n";
	foreach ($menus as $menu) {
		if ($menu['action'] == $active) {
			$output .= '<li class="active">'.l($menu['name'], $menu['link'], array('class' => 'active'));
		} else {
			$output .= '<li>'.l($menu['name'], $menu['link']);
		}
		$output .= '</li>'."\n";
	}
	$output .= '</ul></div>'."\n";
	return $output;
}

/**
 * _ticket_unit_group_list: azoknak a unitoknak es csoportoknak a listaja
 * amikhez van megadott jogosultsaga a felhasznalonak
 * @param: $access - jogosultsag (default: 'ticket administer')
 * @param: $separator - elvalaszto karakter(ek) (default: '/')
 * @param: $addin - az all ertek hozza adasra keruljon-e (default: 0)
 * @param: $active - az aktiv unit es csoport keruljon be (default: 1)
 * @param: $desc - unit es csoport desc mezojet is tartalmazza (default: 0)
 * @return $ug_list - unit group lista
 */
function _ticket_unit_group_list($access = 'ticket administer', $separator = '/', $addin = 0, $active = 1, $desc = 0) {
	global $user;

	// ticket admin jogosultsag listazasa
	$SQL = "SELECT tp.role_id, tp.u_id, tp.gid FROM {ticket_permission} tp INNER JOIN {permission} p ON tp.role_id = p.rid AND role_id IN (%s) WHERE perm LIKE '%%%s%' ORDER BY tp.u_id, tp.gid";
	$result = db_query($SQL, implode(',', array_keys($user->roles)), $access);

	// ticket jogosultsagok tombbe mentese
	$roles = array();
	while ($row = db_fetch_array($result)) {
		if ( (!isset($roles[0][0])) || !($roles[0][0]) ) {
			if ( (!isset($roles[$row['u_id']][0])) || !($roles[$row['u_id']][0]) ) {
				$roles[$row['u_id']][$row['gid']] = 1;
			}
		}
	}
	// u/gid_list = u/gid azonositok ide kerulnek
	$uid_list = array();
	$gid_list = array();
	$SQL_extra = '1';

	if ( count($roles) ) {
		// global admin eseten erre semmi szukseg 
		$u_keys = array_keys($roles);
		if ($u_keys[0] != 0) {
			foreach ($u_keys as $uid) {
				$g_keys = array_keys($roles[$uid]);
				// u_id kigyujtese unit adminnal
				if ($g_keys[0] == 0) {
					$uid_list[] = $uid;
				} else {
				// gid kigyujtese group adminnal
					$gid_list = array_merge($gid_list, $g_keys);
				}
			}

			// SELECT modositas az admin jogoknak megfeleloen
			if (count($uid_list)) {
				$uid_list = implode(',', $uid_list);
				$SQL_extra = 'g.u_id IN (%s)';
				if (count($gid_list)) {
					$gid_list = implode(',', $gid_list);
					$SQL_extra .= ' OR g.gid IN (%s)';
				}
			} elseif(count($gid_list)) {
				$uid_list = implode(',', $gid_list);
				$gid_list = array();
				$SQL_extra = 'g.gid IN (%s)';
			}
		}

		// Ticket unit es group amit adminisztralhat
		$SQL = "SELECT u.u_id, g.gid, u.unit_name, g.group_name, u.description AS u_desc, g.description AS g_desc";
		$SQL .= " FROM {ticket_unit} u, {ticket_group} g WHERE u.u_id = g.u_id AND (".$SQL_extra.")";
		if ($active) {
			$SQL .= " AND u.active = 1 AND g.active = 1 ";
		}
		$SQL .= " ORDER BY u.weight, g.weight, u.unit_name, g.group_name";
		$result = db_query($SQL, $uid_list, $gid_list);
		if ( ($roles[0][0]) && ($addin > 0) ) {
			$ug_list['0'.$separator.'0'] = t('All');
		}
		$last_unit = '';
		if ( $desc ) {
			while ($row = db_fetch_array($result)) {
				if ($last_unit != $row['unit_name']) {
					$last_unit = $row['unit_name'];
					if ( $roles[0][0] || $roles[$row['u_id']][0]) {
						$ug_list[$row['u_id']][0]['name'] = $row['unit_name'];
						$ug_list[$row['u_id']][0]['desc'] = $row['u_desc'];
					} elseif ( $addin < 0) {
						$ug_list[$row['u_id']][0]['name'] = $row['unit_name'];
						$ug_list[$row['u_id']][0]['desc'] = $row['u_desc'];
					}
				}
				if ( $addin > 0 ) {
					$ug_list[$row['u_id']][$row['gid']]['name'] = $row['unit_name'].' / '.$row['group_name'];
					$ug_list[$row['u_id']][$row['gid']]['desc'] = $row['g_desc'];
				} else {
					$ug_list[$row['u_id']][$row['gid']]['name'] = $row['group_name'];
					$ug_list[$row['u_id']][$row['gid']]['desc'] = $row['g_desc'];
				}
			}
		} else {
			while ($row = db_fetch_array($result)) {
				if ($last_unit != $row['unit_name']) {
					$last_unit = $row['unit_name'];
					if ( $roles[0][0] || $roles[$row['u_id']][0]) {
						if ( $addin > 0 ) {
							$ug_list[$row['u_id'].$separator.'0'] = $row['unit_name'];
						} else {
							$ug_list[$row['u_id']] = $row['unit_name'];
						}
					} elseif ( $addin < 0) {
						$ug_list[$row['u_id']] = $row['unit_name'];
					}
				}
				if ( $addin > 0 ) {
					$ug_list[$row['u_id'].$separator.$row['gid']] = $row['unit_name'].' / '.$row['group_name'];
				} else {
					$ug_list[$row['u_id'].$separator.$row['gid']] = $row['group_name'];
				}
			}
		}
	}
	return $ug_list;
}

/**
 * _ticket_chk_emails: e-mail felsorolasban levo mailcimek ellenorzese.
 * @param: $emails - e-mail cimek
 */
function _ticket_chk_emails($emails) {
	$emails =  str_replace(',', ' ', $emails);
	$emails =  str_replace('  ', ' ', $emails);
	$emails = explode(' ', $emails);
	$ret = TRUE;

	foreach ($emails as $email) {
		$email = trim($email);
		if ( ($email != '') && !(valid_email_address($email)) ) {
			$ret = FALSE;
		}
	}
	return $ret;
}

/*********************************************************************
 ***************************** TICKET ID *****************************
 ********************************************************************/

/**
 * ticket_ticketid: a megadott tickethez ugrik
 * @param: $tid - ticket id
 */
function ticket_ticketid($tid) {
	$result = db_query("SELECT tg.u_id, tg.gid FROM {ticket_ticket} tt, {ticket_group} tg WHERE tt.tid = %d AND tt.gid = tg.gid", $tid);
	if ($row = db_fetch_array($result)) {
		drupal_goto('ticket/'.$row['u_id'].'/'.$row['gid'].'/'.$tid);
	} else {
//		drupal_goto('ticket');
		return t('Non-existing ticket ID.');
	}
}

/*********************************************************************
 ******************************* TICKET ******************************
 ********************************************************************/

/**
 * ?q=ticket - es almenui eseten kerul betoltesre
 */
if (arg(0) == 'ticket') {
	include_once drupal_get_path('module', 'ticket') . '/ticket_ticket.inc';
}

/*********************************************************************
 **************************** TICKET ADMIN ***************************
 ********************************************************************/
/**
 * ?q=admin/ticket - es almenui eseten kerul betoltesre
 */
if ( (arg(0) == 'admin') && (arg(1) == 'ticket') ) {
	include_once drupal_get_path('module', 'ticket') . '/ticket_admin.inc';
}

/*********************************************************************
 ************************** TICKET ENCODING **************************
 ********************************************************************/

/**
 * xmlcharrefreplace -> utf8
 */
if ( module_exist('ticket') ) {
	if (function_exists('recode')) {
		$result = db_query("SELECT tid, summary, detail FROM {ticket_ticket} WHERE encode = 1");
		while ($row = db_fetch_array($result)) {
			$row['summary'] = recode("XML-standalone..utf-8", $row['summary']);
			$row['detail'] = recode("XML-standalone..utf-8", $row['detail']);
			db_query("UPDATE {ticket_ticket} SET summary = '%s', detail = '%s', encode = 0 WHERE tid = %d", $row['summary'], $row['detail'], $row['tid']);
		}
	
		$result = db_query("SELECT qid, summary, detail FROM {ticket_queue} WHERE encode = 1");
		while ($row = db_fetch_array($result)) {
			$row['summary'] = recode("XML-standalone..utf-8", $row['summary']);
			$row['detail'] = recode("XML-standalone..utf-8", $row['detail']);
			db_query("UPDATE {ticket_queue} SET summary = '%s', detail = '%s', encode = 0 WHERE qid = %d", $row['summary'], $row['detail'], $row['qid']);
		}
	}
}

?>
