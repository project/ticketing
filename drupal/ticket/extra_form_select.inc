<?php
/*
 * Created on 2006.07.18.
 */
/*
$class = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
$opt = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
$form['log']['host2'] = array(
		'#type' => 'myselect',
		'#input' => TRUE,
		'#title' => t('Host color'),
		'#default_value' => $def['host'],
		'#options' => $opt,
		'#class' => $class,
		'#required' => false,
		'#prefix' => '<div class="wlm-left">',
		'#suffix' => '</div>',
	);
*/


function theme_selectcolor($element) {
	$select = '';
	$size = $element['#size'] ? ' size="' . $element['#size'] . '"' : '';
	_form_set_class($element, array('form-select'));
	return theme('form_element', $element['#title'], '<select name="'. $element['#name'] .''. ($element['#multiple'] ? '[]' : '') .'"'. ($element['#multiple'] ? ' multiple="multiple" ' : '') . drupal_attributes($element['#attributes']) .' id="' . $element['#id'] .'" '. $size .'>'. form_selectcolor_options($element) .'</select>', $element['#description'], $element['#id'], $element['#required'], form_get_error($element));
}

function form_selectcolor_options($element, $choices = NULL) {
	if (!isset($choices)) {
		$choices = $element['#options'];
	}
	// array_key_exists() accommodates the rare event where $element['#value'] is NULL.
	// isset() fails in this situation.
	$value_valid = isset($element['#value']) || array_key_exists('#value', $element);
	$value_is_array = is_array($element['#value']);
	$options = '';
	$class = '';
	foreach ($choices as $key => $choice) {
		if (is_array($choice)) {
			$options .= '<optgroup label="'. $key .'">';
			$options .= form_myselect_options($element, $choice);
			$options .= '</optgroup>';
		} else {
			$key = (string)$key;
			if ($value_valid && ($element['#value'] == $key || ($value_is_array && in_array($key, $element['#value'])))) {
				$selected = ' selected="selected"';
			} else {
				$selected = '';
			}
			if ( isset($element['#class'][$key]) ) {
				$class = 'class = "'.$element['#class'][$key].'"';
			}
			$options .= '<option value="'. $key .'"'. $selected .' '.$class.'>'. check_plain($choice) .'</option>';
		}
	}
	return $options;
}
/*
// EZ MIERT NEM FUT LE ????
function pol_tracker_elements() {
var_dump('dasdas');
  $type['checkbox_columns'] = array('#input' => TRUE, '#process' => array('expand_checkbox_columns' => array()), '#tree' => TRUE);
  return $type;
}
*/

function expand_checkbox_columns($element) {
	$value = is_array($element['#value']) ? $element['#value'] : array();
	$element['#type'] = 'checkboxes';
	$element['#tree'] = TRUE;
	if (count($element['#options']) > 0) {
		if (!isset($element['#default_value']) || $element['#default_value'] == 0) {
			$element['#default_value'] = array();
		}
		$column = 0;
		foreach ($element['#options'] as $key => $choice) {
			$class = ($column % $element['#columns']) && $column ? 'checkbox-columns' : 'checkbox-columns-clear';
			if (!isset($element[$key])) {
				$element[$key] = array('#type' => 'checkbox', '#processed' => TRUE, '#title' => $choice, '#default_value' => in_array($key, $value), '#attributes' => $element['#attributes'], '#prefix' => '<div class="' . $class . '">', '#suffix' => '</div>');
			}
			$column++;
		}
	}
	return $element;
}
















?>
