<?php
/*
 * Created on 2006.07.27.
 */
$ticket_conf = array();
global $ticket_conf;

/* Kotelezo egyet megadni: */
$ticket_conf['def']['type'][] = array(
									'name' => 'scheduled',
									'weight' => 0,
									'def' => 0,
								);
$ticket_conf['def']['type'][] = array(
									'name' => 'unscheduled',
									'weight' => 0,
									'def' => 1,
								);

$ticket_conf['def']['priority'][] = array(
										'name' => 'low',
										'weight' => -5,
										'def' => 0,
									);
$ticket_conf['def']['priority'][] = array(
										'name' => 'normal',
										'weight' => -0,
										'def' => 1,
									);
$ticket_conf['def']['priority'][] = array(
										'name' => 'high',
										'weight' => 5,
										'def' => 0,
									);
$ticket_conf['def']['priority'][] = array(
										'name' => 'urgent',
										'weight' => 9,
										'def' => 0,
									);

$ticket_conf['def']['status']['open'][] = array(
											'name' => 'open',
											'weight' => 0,
											'def' => 1,
										);
$ticket_conf['def']['status']['closed'][] = array(
											'name' => 'closed',
											'weight' => 0,
											'def' => 1,
										);

$ticket_conf['def']['template'] = array(
	'mailto' => '',
	'mailfrom' => '',
	'tsubject' => 'Ticket[%g]-%tid-P:%pri-[%ss]',
	'esubject' => '',
	'usubject' => 'Unchanged Tickets',
	'sep' => '',
	'e_type' => 0,
	'e_pos' => 1,

	'tt_header' => 'Ticket Number : %tid          Ticket Status   : %st
Ticket Type   : %type         Ticket Priority : %pri
Ticket Scope  : %sc           Ticket Site     : %si
Ticket Owner  : %tc           Problem Fixer   : %fixer
Assigned      : %assigned

Ticket Date and Time:
---------------------
Problem started : %ps
Problem legth   : %pl
Problem fixed   : %pf'."\n",

	'tt_body' => 'Problem Description:
%detail

Affected:
%affected

Actions:
%actions

Time To Fix:
%ttf

Fix:
%fix'."\n",

	'tt_footer' => '----------------------------
Phone : phone number
Fax   : fax number
Email : e-mail address',

	'etemplate' => '%edate:
%edesc'."\n",

	'utemplate' => '%tid  - %st  - %ss'."\n",
);

/* Nem kotelezok: */
//$ticket_conf['def']['mail'][] = '';
//$ticket_conf['def']['fixer'][] = array(
//									'name' => '',
//									'weight' => 0,
//									'def' => 1,
//								);
//$ticket_conf['def']['site'][]  = array(
//									'name' => '',
//									'weight' => 0,
//									'def' => 1,
//								);
//$ticket_conf['def']['scope'][]  = array(
//									'name' => '',
//									'weight' => 0,
//									'def' => 1,
//								);


$ticket_conf['limit'] = 50;
$ticket_conf['page_refresh'] = 600;
//$ticket_conf[''] = '';

$ticket_conf['stage'] = array(t('normal'),
	t('%num. stage', array('%num' => 1)),
	t('%num. stage', array('%num' => 2)),
	t('%num. stage', array('%num' => 3)),
	t('%num. stage', array('%num' => 4)),
	t('%num. stage', array('%num' => 5)),
);
//var_dump($ticket_conf['stage']);
//var_dump(t('%num. stage', array('%num' => 1)));
?>
