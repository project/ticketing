
if (isJsEnabled()) {
  addLoadEvent(add_onChange_event);
  addLoadEvent(disabled_Ticket);
  addLoadEvent(Ticket_list);
}

function add_onChange_event() {
	if (document.forms[0].elements['edit-user']) {
		document.getElementById('edit-user').onchange = function() {
			var pos = document.documentURI.lastIndexOf("/");
			var num = document.documentURI.substring(pos+1,document.documentURI.length);
			if (isNaN(num)) {
				location.href=document.documentURI+'/'+this.options[this.selectedIndex].value;
			} else {
				location.href=document.documentURI.substring(0,pos)+'/'+this.options[this.selectedIndex].value;
			}
			return true;
		};
	}
}

function disabled_Ticket() {
	if ( (document.forms[0].elements['edit-assigned']) && (document.forms[0].elements['edit-start']) ) {
		if (document.forms[0].elements['edit-assigned'].value != 0) {
			document.getElementById('edit-assigned').disabled = true;
			if (document.forms[0].elements['edit-start'].value != '') {
				document.getElementById('edit-start').disabled = true;
			}
		}
	}
}

function Ticket_list() {
	if ( (document.forms[0].elements['edit-assigned']) && (document.forms[0].elements['edit-type']) && (document.forms[0].elements['edit-status']) && (document.forms[0].elements['refresh-submit']) ) {
		document.getElementById('edit-sstatus').onchange = function() {
			document.getElementById('ticket_list_form').submit();
		};
		document.getElementById('edit-status').onchange = function() {
			document.getElementById('ticket_list_form').submit();
		};
		document.getElementById('edit-type').onchange = function() {
			document.getElementById('ticket_list_form').submit();
		};
		document.getElementById('edit-assigned').onchange = function() {
			document.getElementById('ticket_list_form').submit();
		};

		document.getElementById('edit-assigned').disabled = false;
		document.getElementById('refresh-submit').disabled = true;
	}
}

