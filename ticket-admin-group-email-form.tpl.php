<?php
// Add table javascript.
?>

<fieldset class=" collapsible"><legend><?php echo t('List e-mails'); ?></legend>
<table id="groups" class="sticky-enabled">
	<thead>
		<tr>
			<th><?php echo t('Default'); ?></th>
			<th><?php echo t('E-mail name'); ?></th>
			<th><?php echo t('E-mail address'); ?></th>
			<th><?php echo t('Enabled'); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php
		$row = 0;
		foreach (element_children($form) as $var) {
			if ( is_numeric($var) ) {
				?>
		<tr class="draggable <?php echo $row % 2 == 0 ? 'odd' : 'even'; ?>">
			<td><?php echo drupal_render($form[$var]['def']) ?></td>
			<td><?php echo drupal_render($form[$var]['name']) ?></td>
			<td><?php echo drupal_render($form[$var]['mailto']) ?></td>
			<td><?php echo drupal_render($form[$var]['active']) ?></td>
		</tr>
		<?php
				$row++;
			}
		} ?>
	</tbody>
</table>

<?php echo drupal_render($form); ?>
</fieldset>
