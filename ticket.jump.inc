<?php
/**
 * ticket_ticketid: a megadott tickethez ugrik
 * @param: $tid - ticket id
 */
function ticket_ticketid($tid) {
	global $user;
	$result = db_query("SELECT tg.u_id, tg.gid FROM {ticket_ticket} tt, {ticket_group} tg WHERE tt.tid = %d AND tt.gid = tg.gid", $tid);
	if ($row = db_fetch_array($result)) {
		if ( ticket_access('ticket reader', $row['u_id'], $row['gid']) ) {
			drupal_goto('ticket/'.$row['u_id'].'/'.$row['gid'].'/'.$tid);
		} else {
			drupal_access_denied();
			return;
		}
	} else {
		return t('Non-existing ticket ID.');
	}
}


