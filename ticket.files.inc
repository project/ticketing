<?php

// TODO: kommentezni mindent mindenhol

function _ticket_files_form($ticket, $u_id, $gid, $tid, $eid) {
	global $user;

	$form = array(
	    '#theme' => 'ticket_files_form_new',
	    '#cache' => TRUE,
	);

	if (!empty($ticket['files']) && is_array($ticket['files'])) {
			$form['files']['#theme'] = 'ticket_files_form_curent';
		$form['files']['#tree'] = TRUE;
		foreach ($ticket['files'] as $key => $file) {
//	watchdog('content', print_r($file, true), array(), WATCHDOG_NOTICE, 'itt2');
			if ( !empty($file['filename']) ) {
				if ( isset($file['filepath']) ) {
					$filename = "<small>". t('Uploading!') ."</small>";
				} else {
					$filename = url('ticket/files/'.$file['fid'].'/'.$file['filename'], array('absolute' => TRUE));
					$filename = "<small>". l($filename, $filename, array('attributes' => array('target' => '_blank'))) ."</small>";
					
				}
				$form['files'][$key]['filename'] = array(
					'#type' => 'textfield',
					'#default_value' => $file['filename'],
					'#maxlength' => 256,
					'#description' => $filename,
				);
				$form['files'][$key]['size'] = array(
					'#value' => format_size($file['filesize']),
				);
				$form['files'][$key]['list'] = array(
					'#type' => 'checkbox',
					'#default_value' => $file['list'],
				);
				$form['files'][$key]['remove'] = array(
					'#type' => 'checkbox',
					'#default_value' => !empty($file['remove']),
				);
	//			$form['files'][$key]['filename'] = array(
	//				'#type' => 'value',
	//				'#value' => $file['filename'],
	//			);
				$form['files'][$key]['filepath'] = array(
					'#type' => 'value',
					'#value' => $file['filepath'],
				);
				$form['files'][$key]['filemime'] = array(
					'#type' => 'value',
					'#value' => $file['filemime'],
				);
				$form['files'][$key]['filesize'] = array(
					'#type' => 'value',
					'#value' => $file['filesize'],
				);
				$form['files'][$key]['fid'] = array(
					'#type' => 'value',
					'#value' => $file['fid'],
				);
			}
		}
	}

	if ( ticket_access('ticket upload files', $u_id, $gid) ) {
		$form['new']['#weight'] = 10;
		$form['new']['ticket_upload'] = array(
			'#type' => 'file',
			'#title' => t('Attach new file'),
			'#size' => 40,
		);
		$e_name = '';
		if ( $eid === 'new') {
			$e_name = '-event';
		}
		$form['new']['attach'.$e_name] = array(
			'#type' => 'submit',
			'#value' => t('Attach'),
			'#name' => 'attach'.$e_name,
			'#ahah' => array(
				'path' => 'ticket/js/files/'.$u_id.'/'.$gid.'/'.$tid.'/'.$eid,
				'wrapper' => 'attach'.$e_name.'-wrapper',
				'progress' => array('type' => 'bar', 'message' => t('Please wait...')),
		),
			'#submit' => array('ticket_files_form_submit_build'),
		);

	}

	// This value is used in ticket_upload_js().
	return $form;
}

/**
 * Theme the attachment form.
 * Note: required to output prefix/suffix.
 *
 * @ingroup themeable
 */
function theme_ticket_files_form_new($form) {
  drupal_add_tabledrag('ticket_upload-attachments', 'order', 'sibling', 'ticket_upload-weight');
  $output = drupal_render($form);
  return $output;
}

/**
 * Theme the attachments list.
 * @ingroup themeable
 */
function theme_ticket_files_form_curent(&$form) {
	$header = array('', t('Delete'), t('Public'), t('File name'), t('Size'));

	foreach (element_children($form) as $key) {
		$row = array('');
		$row[] = drupal_render($form[$key]['remove']);
		$row[] = drupal_render($form[$key]['list']);
		$row[] = drupal_render($form[$key]['filename']);
		$row[] = drupal_render($form[$key]['size']);
		$rows[] = array('data' => $row);
	}
	$output = theme('table', $header, $rows, array('class' => 'ticket-files-attachments'));
	$output .= drupal_render($form);
	return $output;
}

/**
 * ticket_file_form_submit_build
 */
function ticket_files_form_submit_build($form, &$form_state) {
	// Unset any button-level handlers, execute all the form-level submit
	// functions to process the form values into an updated node.
	unset($form_state['submit_handlers']);
	form_execute_handlers('submit', $form, $form_state);
	$form_state['rebuild'] = TRUE;
	return $form_state['values'];
}


/**
 * Menu-callback for JavaScript-based ticket_uploads.
 */
function ticket_files_js($u_id, $gid, $tid, $eid) {
	// Load the form from the Form API cache.
	$cache = cache_get('form_'. $_POST['form_build_id'], 'cache_form');

//	watchdog('content', print_r($_POST, true), array(), WATCHDOG_NOTICE, 'itt1');
	
	$form = $cache->data;
	$form_state = array('values' => $_POST);
	$form_state['values']['u_id'] = $u_id;
	$form_state['values']['gid'] = $gid;
	$form_state['values']['tid'] = $tid;
	$form_state['values']['eid'] = $eid;
	
	// Handle new ticket_uploads, and merge tmp files into node-files.
	ticket_files_form_submit($form, $form_state);
	$ticket_files = array();
	
	if ( $eid ) {
		$ticket_files = ticket_files_load($tid, $eid);
	} else {
		$ticket_files = ticket_files_load($tid);
	}
		
	if (!empty($form_state['values']['files'])) {
		foreach ($form_state['values']['files'] as $fid => $file) {
			if (is_numeric($fid)) {
				$form_state['values']['files'][$fid] = $file;
				if (!isset($file['filepath'])) {
					$form_state['values']['files'][$fid] = $ticket_files[$fid];
				}
			}
		}
	}
	$form = _ticket_files_form($form_state['values'], $u_id, $gid, $tid, $eid);

	// Update the default values changed in the $form_state['values'] array.
	$files = isset($form_state['values']['files']) ? $form_state['values']['files'] : array();
	foreach ($files as $fid => $file) {
		if (is_numeric($fid)) {
			$form['files'][$fid]['filename']['#default_value'] = $file['filename'];
			$form['files'][$fid]['list']['#default_value'] = isset($file['list']) ? 1 : 0;
			$form['files'][$fid]['remove']['#default_value'] = isset($file['remove']) ? 1 : 0;
		}
	}

	// Add the new element to the stored form state and resave.
	if ($_POST['form_id'] == 'ticket_add_event') {
		$cache->data['sub']['attachments']['wrapper'] = array_merge($cache->data['sub']['attachments']['wrapper'], $form);
	} else {
		$cache->data['attachments']['wrapper'] = array_merge($cache->data['attachments']['wrapper'], $form);
	}
	cache_set('form_'. $_POST['form_build_id'], $cache->data, 'cache_form', $cache->expire);

	// Render the form for output.
	$form += array(
	    '#post' => $_POST,
	    '#programmed' => FALSE,
	    '#tree' => FALSE,
	    '#parents' => array(),
	);
	drupal_alter('form', $form, array(), 'ticket_files_js');
	$form_state = array('submitted' => FALSE);
	$form = form_builder('ticket_files_js', $form, $form_state);
	$output = theme('status_messages') . drupal_render($form);
	
	// We send the updated file attachments form.
	// Don't call drupal_json(). ahah.js uses an iframe and
	// the header output by drupal_json() causes problems in some browsers.
	print drupal_to_js(array('status' => TRUE, 'data' => $output));
	exit;
}

function ticket_files_form_submit($form, &$form_state) {
	global $user;

	$values = $form_state['values'];
	
	// Save new file ticket_uploads.
	if ( (ticket_access('ticket upload files', $values['u_id'], $values['gid']))  && ($file = file_save_upload('ticket_upload')) ) {
		$file = (array)$file;
		$file['list'] = 1;
		$_SESSION['ticket_upload_files'][ $values['u_id'] ][ $values['gid'] ][ $values['tid'] ][ $values['eid'] ][$file['fid']] = $file;
	}
	
	// Attach session files to node.
	if (!empty($_SESSION['ticket_upload_files'][ $values['u_id'] ][ $values['gid'] ][ $values['tid'] ][ $values['eid'] ])) {
		foreach ($_SESSION['ticket_upload_files'][ $values['u_id'] ][ $values['gid'] ][ $values['tid'] ][ $values['eid'] ] as $fid => $file) {
			if (!isset($form_state['values']['files'][$fid]['filepath'])) {
				$form_state['values']['files'][$fid] = $file;
			}
		}
	}
}

function ticket_files_load($tid, $eid = 0) {
	$files = array();

	$result = db_query('SELECT fid, tid, eid, uid, filename, filemime, filesize, timestamp, list FROM {ticket_files} WHERE tid = %d AND eid = %d ORDER BY filename, fid', $tid, $eid);
	while ($file = db_fetch_array($result)) {
		$files[$file['fid']] = $file;
	}
	return $files;
}

function ticket_files_save($fid) {
	$SQL = "SELECT tg.u_id, tg.gid, tf.list FROM {ticket_files} tf, {ticket_ticket} t, {ticket_group} tg";
	$SQL .= " WHERE tf.fid = %d AND tf.tid = t.tid AND t.gid = tg.gid";
	$result = db_query($SQL, $fid);
	if ($row = db_fetch_array($result)) {
		if ( ticket_access('ticket upload files', $row['u_id'], $row['gid']) || ( ticket_access('view ticket uploaded files', $row['u_id'], $row['gid']) && $row['list'] ) ) {
			ob_end_clean();
			$rs = db_query("SELECT filename, filemime, data FROM {ticket_files} WHERE fid = %d", $fid);
			$file = db_fetch_array($rs);
			drupal_set_header('Content-Type: '.$file['filemime']);
			drupal_set_header('Content-Disposition: filename="'.$file['filename'].'";');
			echo $file['data'];
			die;
		} else {
			return drupal_access_denied();
		}
	} else {
		return drupal_not_found();
	}
}
