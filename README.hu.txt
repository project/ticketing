Telepítési útmutató hibajegykezelő rendszerhez.

0. Hibajegykezelő licensze:
  GNU GPL v2.0

1. A program használatához szükséges szoftverek:
- php-recode
- Drupal 6.x letölthető: http://drupal.org/project/Drupal+project címen.
- sqlalchemy 0.3.0 vagy újabb (letölthető: http://www.sqlalchemy.org/download.myt)
  Ezen szoftverek telepítésére nem térnék ki, található hozzájuk telepítési
  útmutató.
- Hibajegykezelő letölthető: http://drupal.org/project/ticketing címen.

2. Kicsomagolás, előkészületek:
 $ tar -xvvzf ticketing-6.x-*.tar.gz
 $ cd ticketing
 Hibajegykezelő modul másolása a drupal modules mappájába pl.:
  (Ha a drupal a /var/www/drupal mappába került telepítésre)
 $ cp -r ticketing/  /var/www/drupal/sites/all/modules/ticket

3. Telepítés:
 3.1 Hibajegykezelő modul telepítése/aktiválása:
  Engedélyezzük a hibajegykezelő modult a drupalban, ezt a következőkép tehetjük:
  (Ha a drupalt a következő helyre telepítettük: http://localhost/drupal/)
  Látogassuk meg következő oldalt: http://localhost/drupal/?q=admin/modules itt
  válaszuk ki a listából a ticket modult majd mentsük el a beállításokat.
  Ezután látogassuk meg a következő oldalt: http://localhost/drupal/?q=admin/ticket
  Hozzunk létre az első szervezeti egységünket: http://localhost/drupal/?q=admin/ticket/add/unit
  Majd az első csoportot: http://localhost/drupal/?q=admin/ticket/group/add/1
  Végezzük el a csoporthoz tartozó beállításokat: http://localhost/drupal/?q=/admin/ticket/group/edit/1
  A modulhoz tartozó jogosultságokat itt állíthatjuk: http://localhost/drupal/?q=admin/ticket/access
  Ha mindent beállítottunk akkor hozzuk létre a hibajegyeinket: http://localhost/drupal/?q=ticket
  A hibajegyeinkre közvetlenül is hivatkozhatunk: http://localhost/drupal/?q=ticketid/[szám]

 3.2 E-mail feldolgozó telepítése/aktiválása:
  Másoljuk a nekünk tetsző helyre pl.:
  $ cp ticketing/e-mail/mail2ticket.py /usr/local/bin/
  Ezután vegyünk fel egy sort az /etc/aliases fileba a következő kép:
  [e-mail cím amire a levél érkezik]: [”|ahova a filet másoltuk+filenév <e-mail cím ami megegyezik a 'Group e-mail address:'-hoz beírt címmel>”]
  Lássunk erre egy példát:
  ticket: "|/usr/local/bin/mail2ticket.py ticket@example.org"

4. Felesleges állományok törlése:
 Hibajegykezelő drupal modul könyvtárából töröljük a felesleges állományokat:
  (Ha a drupal a /var/www/drupal mappába került telepítésre)
 $ rm -r /var/www/drupal/sites/all/modules/ticket/doc
 $ rm -r /var/www/drupal/sites/all/modules/ticket/e-mail
 $ rm -r /var/www/drupal/sites/all/modules/ticket/utils
