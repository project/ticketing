<?php
// Add table javascript.
?>

<fieldset class=" collapsible"><legend><?php echo t('List status'); ?></legend>
<table id="groups" class="sticky-enabled">
	<thead>
		<tr>
			<th><?php echo t('Status'); ?></th>
			<th><?php echo t('Closed'); ?></th>
			<th><?php echo t('Enabled'); ?></th>
			<th><?php echo t('Weight'); ?></th>
			<th><?php echo t('Stage'); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php
		$row = 0;
		foreach (element_children($form) as $var) {
			if ( is_numeric($var) ) {
				?>
		<tr class="draggable <?php echo $row % 2 == 0 ? 'odd' : 'even'; ?>">
			<td><?php echo drupal_render($form[$var]['name']) ?></td>
			<td><?php echo drupal_render($form[$var]['closed']) ?></td>
			<td><?php echo drupal_render($form[$var]['active']) ?></td>
			<td><?php echo drupal_render($form[$var]['weight']) ?></td>
			<td><?php echo drupal_render($form[$var]['stage']) ?></td>
		</tr>
		<?php
				$row++;
			}
		} ?>
	</tbody>
</table>

<?php echo drupal_render($form); ?>
</fieldset>
