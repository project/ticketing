<?php
// Add table javascript.
?>

<table id="groups" class="sticky-enabled">
	<thead>
		<tr>
			<th><?php echo t('Group'); ?></th>
			<th><?php echo t('Weight'); ?></th>
			<th colspan="2"><?php echo t('Operations'); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php
		$row = 0;
		foreach (element_children($form) as $unit) {
			if ( is_numeric($unit) ) {
				foreach (element_children($form[$unit]) as $key => $value) {
					if ($value == 'name') {
						?>
		<tr class="group group-<?php print $key; echo $row % 2 == 0 ? ' odd' : ' even'; ?>" >
			<td colspan="2" class="group"><?php print drupal_render($form[$unit]['name']); ?></td>
			<td colspan="2" class="group"><?php print drupal_render($form[$unit]['add']); ?></td>
		</tr>
						<?php
						$row++;
					}
					
					if ( is_numeric($value) ) {
				?>
		<tr class="draggable <?php echo $row % 2 == 0 ? 'odd' : 'even'; ?>">
			<td class="group"><div class="indentation">&nbsp;</div><?php echo drupal_render($form[$unit][$value]['info']) ?></td>
			<td><?php echo drupal_render($form[$unit][$value]['weight']); ?></td>
			<td><?php echo drupal_render($form[$unit][$value]['configure']) ?></td>
			<td><?php echo drupal_render($form[$unit][$value]['active']) ?></td>
		</tr>
		<?php
						$row++;
					}
				}
			}
		} ?>
	</tbody>
</table>

<?php echo drupal_render($form); ?>