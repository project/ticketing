<?php
// Add table javascript.
drupal_add_css(drupal_get_path('module', 'ticket').'/ticket.css');
$t_perms = ticket_perms();
?>

<table id="t_access" class="sticky-enabled">
	<thead>
		<tr>
			<th><?php echo t('Scope'); ?></th>
			<?php
			foreach ($t_perms as $perm) {
				echo '<th>'.t(ucfirst($perm)).'</th>';
			}
			?>
			<th><?php echo t('Operations'); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php
		$row = 0;
		foreach (element_children($form) as $i):
			if ( !empty($form[$i]['name']) ):
			
		?>
		<tr class=" <?php echo $row % 2 == 0 ? 'odd' : 'even'; ?>">
			<td class="td_access"><?php echo drupal_render($form[$i]['name']) ?></td>
			<?php
			foreach ($t_perms as $perm) {
				echo '<td>'.drupal_render($form[$i][$perm]).'</td>';
			}
			?>
			<td><?php echo drupal_render($form[$i]['delete']) ?></td>
		</tr>
		<?php
			endif;
			$row++;
		endforeach; ?>
	</tbody>
</table>

<?php echo drupal_render($form); ?>
