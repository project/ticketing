<?php
// Add table javascript.
switch ($form['type']["#value"]) {
	case 'fixer':
		$table_txt = t('Fixer');
		$legend_txt = t('List fixer');
	break;

	case 'priority':
		$table_txt = t('Priority');
		$legend_txt = t('List priority');
	break;

	case 'scope':
		$table_txt = t('Scope');
		$legend_txt = t('List scope');
	break;

	case 'site':
		$table_txt = t('Site');
		$legend_txt = t('List site');
	break;

	case 'type':
		$table_txt = t('Type');
		$legend_txt = t('List type');
	break;
}

?>

<fieldset class=" collapsible"><legend><?php echo $legend_txt; ?></legend>
<table id="groups" class="sticky-enabled">
	<thead>
		<tr>
			<th><?php echo $table_txt; ?></th>
			<th><?php echo t('Enabled'); ?></th>
			<th><?php echo t('Weight'); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php
		$row = 0;
		foreach (element_children($form) as $var) {
			if ( is_numeric($var) ) {
				?>
		<tr class="draggable <?php echo $row % 2 == 0 ? 'odd' : 'even'; ?>">
			<td><?php echo drupal_render($form[$var]['name']) ?></td>
			<td><?php echo drupal_render($form[$var]['active']) ?></td>
			<td><?php echo drupal_render($form[$var]['weight']); ?></td>
		</tr>
		<?php
				$row++;
			}
		} ?>
	</tbody>
</table>

<?php echo drupal_render($form); ?>
</fieldset>
