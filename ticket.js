if(Drupal.jsEnabled){
	$(document).ready(ticket_init);
}

function ticket_init(){
	$('#edit-user').change(
		function() {
			$.getJSON(Drupal.settings.basePath, {q: 'admin/ticket/access/users/select/' + $(this).val()}, ticket_access_user_select);
		}
	);
	
	$('#edit-simple-status').change(
		function() {
			$('#ticket-list-form').submit();
		}
	);

	$('#edit-status').change(
		function() {
			$('#ticket-list-form').submit();
		}
	);

	$('#edit-type').change(
		function() {
			$('#ticket-list-form').submit();
		}
	);

	$('#edit-assigned').change(
		function() {
			$('#ticket-list-form').submit();
		}
	);
	$('.refresh-submit').css({ visibility: 'hidden'});

}

function ticket_access_user_select(j){
	for(i in j) {
		$("#edit-roles-"+i).attr({ checked: j[i] });
	}
}

