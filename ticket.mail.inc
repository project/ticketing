<?php
/**
 * _ticket_mail_header: create ticket mail header
 */
function _ticket_mail_header(&$message, &$params) {
	// drupal default header
    $message['headers'] = array(
        'MIME-Version'              => '1.0',
        'Content-Type'              => 'text/plain; charset=UTF-8; format=flowed; delsp=yes',
        'Content-Transfer-Encoding' => '8Bit',
        'X-Mailer'                  => 'Drupal'
    );
	$message['from'] = $message['headers']['From'] = $message['headers']['Reply-To'] = 
	$message['headers']['Sender'] =	$message['headers']['Return-Path'] = 
	$message['headers']['Errors-To'] = $params['template']['mail_address'];
	
	if ($message['id'] != 'ticket_new_assigned') {
		// akik masolatot kapnak a levelrol
		$result = db_query("SELECT cc FROM {ticket_ticket} WHERE tid = %d", $params['tid']);
		$row = db_fetch_array($result);
		if ( !empty($row['cc']) ) {
			$message['headers']['CC'] = implode(', ', explode(' ', str_replace(', ', ' ', $row['cc']) ) );
		}
	
		// akiknek kikuldesre kerul a level
		$SQL = "SELECT tm.mailto, tm.email_name FROM {ticket_email} tm, {ticket_rel_ticket_email} trm";
		$SQL .= " WHERE trm.tid = %d AND trm.email_id = tm.email_id";
		$result = db_query($SQL, $params['tid']);
		$mailto = array();
		while ($row = db_fetch_array($result)) {
			$mailto[] = $row['email_name'].' <'.$row['mailto'].'>';
		}
		$message['to'] = implode(', ', $mailto);
	} elseif ($message['id'] == 'ticket_new_assigned') {
		$SQL = "SELECT u.mail FROM {ticket_event} te, {users} u WHERE te.eid = %d AND te.reassigned = u.uid";
		$result = db_query($SQL, $params['eid']);
		$mail = db_fetch_array($result);
		$message['to'] = $mail['mail'];
	}

}

/**
 * _ticket_mail_template_ticket_var: ticket adatok elokeszitese behelyetesiteshez.
 * @param &$params - drupal_mail params
 */
function _ticket_mail_template_ticket_var(&$params) {
	// ticket lekerdezese
	$SQL = "SELECT t.tid, t.uid, t.assigned, tp.priority_name, tt.type_name, tsc.scope_name , tsi.site_name , tf.fixer_name ";
	$SQL .= ", tg.group_name , tu.unit_name, t.summary, t.created, t.start, t.finish, t.detail, t.affected, t.actions, t.time_to_fix, t.fix";
	$SQL .= " FROM {ticket_ticket} t, {ticket_priority} tp, {ticket_type} tt, {ticket_scope} tsc, {ticket_site} tsi, {ticket_fixer} tf,";
	$SQL .= " {ticket_unit} tu, {ticket_group} tg";
	$SQL .= " WHERE t.tid = %d AND t.priority_id = tp.priority_id AND t.type_id = tt.type_id AND t.scope_id = tsc.scope_id";
	$SQL .= " AND t.site_id = tsi.site_id AND t.fixer_id = tf.fixer_id AND t.gid = tg.gid AND tg.u_id = tu.u_id";
	$result = db_query($SQL, $params['tid']);
	$row = db_fetch_array($result);

	$files = array();
	$rs = db_query("SELECT fid, filename, filesize FROM {ticket_files} WHERE tid = %d AND eid = 0", $params['tid']);
	while ( $file = db_fetch_array($rs) ) {
		$files[] = ' - '.url('ticket/files/'.$file['fid'].'/'.$file['filename'], array('absolute' => TRUE)).' ('.format_size($file['filesize']).')';
	}

	$params['template']['var']['%tfiles'] = '';
	if ( count($files) ) {
//		$params['template']['var']['%tfiles'] = t("!!! File attachments - Ticket:\n");
		$params['template']['var']['%tfiles'] .= implode("\n", $files);
	}
	
	// ticket ertekeinek hozzarendelese a minta valtozoihoz
	$params['template']['var']['%tid'] = $row['tid'];
	$params['template']['var']['%g'] = $row['group_name'];
	$params['template']['var']['%u'] = $row['unit_name'];
	$params['template']['var']['%pri'] = $row['priority_name'];
	$params['template']['var']['%type'] = $row['type_name'];
	$params['template']['var']['%sc'] = $row['scope_name'];
	$params['template']['var']['%si'] = $row['site_name'];
	$params['template']['var']['%fixer'] = $row['fixer_name'];
	$params['template']['var']['%ss'] = $row['summary'];
	$params['template']['var']['%ps'] = format_date($row['start'], 'small');
	$params['template']['var']['%detail'] = $row['detail'];
	$params['template']['var']['%affected'] = $row['affected'];
	$params['template']['var']['%actions'] = $row['actions'];
	$params['template']['var']['%ttf'] = $row['time_to_fix'];
	$params['template']['var']['%fix'] = $row['fix'];

	$tc = user_load($row['uid']);
	$params['template']['var']['%tc'] = check_plain($tc->name);
	$assigned = user_load($row['assigned']);
	$params['template']['var']['%assigned'] = check_plain($assigned->name);
	if ( isset($row['finish']) ) {
		$finish = $row['finish'];
		$params['template']['var']['%pf'] = format_date($row['finish'], 'small');
	} else {
		$finish = time();
		$params['template']['var']['%pf'] = " ";
	}
	$params['template']['var']['%pl'] = format_interval($finish - $row['start'], 3);

	$SQL = "SELECT ts.status_name FROM {ticket_event} te, {ticket_rel_event} tre, {ticket_status} ts"; 
	$SQL .= " WHERE tre.tid = %d AND tre.eid = te.eid AND te.status_id = ts.status_id";
	$result = db_query($SQL, $params['tid']);
	$row = db_fetch_array($result);
	$params['template']['var']['%st'] = $row['status_name'];
	// specialis minta helyetesites
	// ha a cserelendo minta mogott annyi ures hely van, hogy a minta erteke odafer akkor ez alapjan lesz lecserelve  
	$keys = array('%tid', '%g', '%u', '%pri', '%type', '%sc', '%si', '%fixer', '%ps', '%pf', '%tc', '%assigned', '%pl', '%st');
	foreach ($keys as $key) {
		$length['key'] = drupal_strlen($key);
		$length['var'] = drupal_strlen($params['template']['var'][$key]);
		if ($length['key'] < $length['var']) {
			$params['template']['space_var'][str_pad($key, $length['var']+1)] = $params['template']['var'][$key].' ';
		} elseif ($length['key'] != $length['var']) {
			$params['template']['space_var'][$key.'  '] = str_pad($params['template']['var'][$key], $length['key']+2);
		}
	}
}

/**
 * _ticket_mail_template_ticket: ticket adatok behelyetesitese a sablonba.
 * @param &$params - drupal_mail params
 */
function _ticket_mail_template_ticket(&$params) {
	
	_ticket_mail_template_ticket_var($params);
	
	// eloszor a specialis minta helyetesites fut le a ticket mintak mindegyikere
	$params['template']['tt_header'] = strtr($params['template']['tt_header'], $params['template']['space_var']);
	$params['template']['tt_body'] = strtr($params['template']['tt_body'], $params['template']['space_var']);
	$params['template']['tt_footer'] = strtr($params['template']['tt_footer'], $params['template']['space_var']);

	// majd a normal helyetesites, a specialis helyetesites a targy mezore nem fut le
	$params['template']['tsubject'] = strtr($params['template']['tsubject'], $params['template']['var']);
	$params['template']['tt_header'] = strtr($params['template']['tt_header'], $params['template']['var']);
	$params['template']['tt_body'] = strtr($params['template']['tt_body'], $params['template']['var']);
	$params['template']['tt_footer'] = strtr($params['template']['tt_footer'], $params['template']['var']);
}

/**
 * _ticket_mail_template_event: ticket event adatok behelyetesitese a sablonba.
 * @param &$params - drupal_mail params
 */
function _ticket_mail_template_event(&$params) {
	if ($params['template']['sep'] != "") {
		$params['template']['sep'] .= "\n";
	}
	$params['template']['sep'] .= "\n";

	// az esemenyek lekerdezese
	$SQL = 'SELECT te.eid, te.uid, te.reassigned, te.created, te.description, ts.status_name FROM {ticket_event} te, {ticket_status} ts';
	$SQL .= " WHERE te.tid = %d  AND te.status_id = ts.status_id ORDER BY created";
	switch ($params['template']['e_type']) {
		// esemenyek novekvo sorrendben
		case 0:
		case 2:
		break;

		// esemenyek csokkeno sorrendben
		case 1:
		case 3:
			$SQL .= " DESC";
		break;

		// csak az utolso esemeny
		case 4:
			$SQL = 'SELECT te.eid, te.uid, te.reassigned, te.created, te.description, ts.status_name';
			$SQL .= ' FROM {ticket_event} te, {ticket_rel_event} tre, {ticket_status} ts';
			$SQL .= ' WHERE tre.tid = %d AND tre.eid = te.eid AND te.status_id = ts.status_id';
		break;
	}
	$result = db_query($SQL, $params['tid']);
	
	// esemenyek egymas moge fuzese
	$params['template']['event'] = '';
	$tmp = array();
	while ($row = db_fetch_array($result)) {
		
		$files = array();
		$rs = db_query("SELECT fid, filename, filesize FROM {ticket_files} WHERE tid = %d AND eid = %d", $params['tid'], $row['eid']);
		while ( $file = db_fetch_array($rs) ) {
			$files[] = ' - '.url('ticket/files/'.$file['fid'].'/'.$file['filename'], array('absolute' => TRUE)).' ('.format_size($file['filesize']).')';
		}
	
		$tmp['%efiles'] = '';
		if ( count($files) ) {
//			$tmp['%efiles'] = t("!!! File attachments - Event:\n");
			$tmp['%efiles'] .= implode("\n", $files);
		}
		
		$tmp['%eid'] = $row['eid'];
		$tmp['%edate'] = format_date($row['created'], 'small');
		$tmp['%edesc'] = $row['description'];
		$tmp['%st'] = $row['status_name'];
		$ec = user_load($row['uid']);
		$tmp['%ec'] = check_plain($ec->name);
		$ra = user_load($row['assigned']);
		$tmp['%ra'] = check_plain($ra->name);
		// ha csokkeno sorrendben vannak rendezve az esemenyek akkor az elso elem adja a level targyat 
		if ( !($params['template']['e_type'] % 2) ) {
			$params['template']['esubject'] = strtr($params['template']['esubject'], $tmp);
			$params['template']['esubject'] = strtr($params['template']['esubject'], $params['template']['var']);
		}

		// specialis behelyetesites elokeszitese
		$keys = array('%eid', '%edate', '%st', '%ra', '%ec');
		foreach ($keys as $key) {
			$length['key'] = drupal_strlen($key);
			$length['var'] = drupal_strlen($params['template']['var'][$key]);
			if ($length['key'] < $length['var']) {
				$evar[str_pad($key, $length['var']+1)] = $tmp[$key].' ';
			} elseif ($length['key'] != $length['var']) {
				$evar[$key.'  '] = str_pad($params['template']['var'][$key], $length['key']+2);
			}
		}

		// behelyetesitesek
		$event = strtr($params['template']['etemplate'], $evar);
		$event = strtr($event, $tmp);
		$event = strtr($event, $params['template']['space_var']);
		$params['template']['event'] .= strtr($event, $params['template']['var']);
		$params['template']['event'] .= "\n".$params['template']['sep'];
	}
	// level targyaba itt helyetesitunk be, ha novekvo sorrend van kivalasztva
	$params['template']['esubject'] = strtr($params['template']['esubject'], $tmp);
	$params['template']['esubject'] = strtr($params['template']['esubject'], $params['template']['var']);
}

function _ticket_mail_template_assigned(&$params) {
	_ticket_mail_template_ticket_var($params);
	
	$SQL = 'SELECT te.eid, te.uid, te.reassigned, te.created, te.description, ts.status_name';
	$SQL .= ' FROM {ticket_event} te, {ticket_status} ts';
	$SQL .= ' WHERE te.eid = %d AND te.status_id = ts.status_id';
	$result = db_query($SQL, $params['eid']);
	$tmp = array();
	if ($row = db_fetch_array($result)) {
		if ( $row['uid'] ) {
			$ec = user_load($row['uid']);
			$params['template']['var']['%ec'] = check_plain($ec->name);
			$params['template']['var']['%edesc'] = $row['description'];
		} else {
			$params['template']['var']['%ec'] = $params['template']['var']['%tc'];
			$params['template']['var']['%edesc'] = t('New ticket.');
		}
		$params['template']['var']['%eid'] = $row['eid'];
		$params['template']['var']['%edate'] = format_date($row['created'], 'small');
		$params['template']['var']['%st'] = $row['status_name'];
		$ra = user_load($row['assigned']);
		$params['template']['var']['%ra'] = check_plain($ra->name);
		// specialis behelyetesites elokeszitese
		$keys = array('%eid', '%edate', '%st', '%ra', '%ec');
		foreach ($keys as $key) {
			$length['key'] = drupal_strlen($key);
			$length['var'] = drupal_strlen($params['template']['var'][$key]);
			if ($length['key'] < $length['var']) {
				$params['template']['space_var'][str_pad($key, $length['var']+1)] = $params['template']['var'][$key].' ';
			} elseif ($length['key'] != $length['var']) {
				$params['template']['space_var'][$key.'  '] = str_pad($params['template']['var'][$key], $length['key']+2);
			}
		}

		// behelyetesitesek
		$params['template']['atemplate'] = strtr($params['template']['atemplate'], $params['template']['space_var']);
		$params['template']['atemplate'] = strtr($params['template']['atemplate'], $params['template']['var']);
		$params['template']['asubject'] = strtr($params['template']['asubject'], $params['template']['var']);
	}
}

