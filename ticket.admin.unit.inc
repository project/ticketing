<?php

/**
 * ticket_admin_unit_list_form: Ticket unit admisztracios listaja
 * @return array - $form: unit list form
 */
function ticket_admin_unit_list_form() {
	$result = db_query("SELECT u_id, unit_name, weight, active FROM {ticket_unit} ORDER BY active DESC, weight ASC, unit_name ASC");
	while ($row = db_fetch_array($result)) {
		/** unit adminisztrator szamara lista keszites */
		$form[$row['u_id']]['info'] = array('#value' => $row['unit_name']); 
		$form[$row['u_id']]['weight'] = array(
			'#type' => 'weight',
			'#default_value' => $row['weight'],
			'#attributes' => array('class' => 'unit-weight unit-weight-'.$row['u_id']),
		);
		if ( ticket_access('ticket administer', $row['u_id'], 0) ) {
			$form[$row['u_id']]['configure'] = array('#value' => l(t('edit unit'), 'admin/ticket/unit/edit/'.$row['u_id']) );
			if ($row['active']) {
				$form[$row['u_id']]['active'] = array('#value' => l(t('disable'), 'admin/ticket/unit/active/'.$row['u_id'].'/0') );
			} else {
				$form[$row['u_id']]['active'] = array('#value' => l(t('enable'), 'admin/ticket/unit/active/'.$row['u_id'].'/1') );
			}
		} else {
			$form[$row['u_id']]['configure'] = array('#value' => t('edit unit')); 
		}
	}

	$form['#action'] = url('admin/ticket/unit/list/');
	$form['#tree'] = TRUE;
	$form['submit'] = array(
		"#type" => "submit",
		"#value" => t('Save configuration'),
	);
	return $form;
}

/**
 * ticket_admin_unit_list_form_submit: unit weight beallitasa 
 */
function ticket_admin_unit_list_form_submit($form, &$form_state) {
	foreach ($form_state['values'] as $u_id => $weight) {
		if ( is_numeric($u_id) ) {
			$SQL = "UPDATE {ticket_unit} SET weight = %d WHERE u_id = %d";
			db_query($SQL, $weight['weight'], $u_id);
		}
	}
}

/**
 * _ticket_admin_unit_form:
 * @param $def - unit form default erteki
 * @return array - $form: unit add/edit form
 */
function _ticket_admin_unit_form($def = array()) {
	$form['name'] = array(
		'#type' => 'textfield',
		'#title' => t('Unit name'),
		'#default_value' => isset($def['unit_name']) ? $def['unit_name'] : NULL,
		'#size' => 60,
		'#maxlength' => 255,
		'#required' => true,
	);

	$form['desc'] = array(
		'#type' => 'textarea',
		'#title' => t('Description'),
		'#default_value' => isset($def['description']) ? $def['description'] : NULL,
		'#required' => false,
	);

	$form['weight'] = array(
		'#type' => 'weight',
		'#title' => t('Weight'),
		"#type" => "weight",
		'#default_value' => isset($def['weight']) ? $def['weight'] : NULL,
	);
	
	$form['active'] = array(
		'#type' => 'select',
		'#title' => t('Active'),
		'#default_value' => isset($def['active']) ? $def['active'] : NULL,
		'#options' => array(
			'1' => t('Yes'),
			'0' => t('No'),
		),
	);
	return $form;
}

/**
 * ticket_admin_add_unit: unit hozzaadas
 */
function ticket_admin_unit_add() {
	$form = _ticket_admin_unit_form(array('weight' => 0));
	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save')
	);
	return $form;
}

/**
 * ticket_admin_add_unit_validate: add unit formrol erkezo adatok ellenorzese 
 */
function ticket_admin_unit_add_validate($form, &$form_state) {
	/** azonos nevu unit nem johet letre */
	$result = db_query("SELECT count(u_id) as num FROM {ticket_unit} WHERE unit_name = '%s'", $form_state['values']['name']);
	$row = db_fetch_array($result);

	if ( $row['num'] ) {
		form_set_error('', t('You must select a name for this unit of settings.'));
	}
}

/**
 * ticket_admin_add_unit_submit: unit hozzaadas
 */
function ticket_admin_unit_add_submit($form, &$form_state) {
	/** unit hozzaadasa */
	$SQL = "INSERT INTO {ticket_unit} (unit_name, description, weight, active) VALUES ('%s', '%s', %d, %d)";
	db_query($SQL, $form_state['values']['name'], $form_state['values']['desc'], $form_state['values']['weight'], $form_state['values']['active']);
	$u_id = db_last_insert_id('{ticket_unit}', 'u_id');

	/** unit menupont letrehozasa a ticket menu alatt */
	$menu = db_fetch_array(db_query("SELECT mlid AS plid, menu_name FROM {menu_links} WHERE link_path = 'ticket'"));
	$menu['router_path'] = 'ticket/%';
 	$menu['module'] = 'ticket';
	$menu['mlid'] = 0;
	$menu['link_title'] = $form_state['values']['name'];
	$menu['link_path'] = 'ticket/'.$u_id;
	if (!db_result(db_query("SELECT mlid FROM {menu_links} WHERE link_path = '%s' AND plid = %d", $menu['link_path'], $menu['plid']))) {
		$mlid = menu_link_save($menu);
		$unit_base = db_fetch_array(db_query("SELECT mlid AS plid, menu_name, module FROM {menu_links} WHERE mlid = %d ", $mlid));
		
		$unit_list = $unit_base;
		$unit_list['mlid'] = $mild;
		$unit_list['router_path'] = 'ticket/%/list';
		$unit_list['link_title'] = 'List';
		$unit_list['link_path'] = 'ticket/'.$u_id.'/list';
		$unit_list['weight'] = 8;
		menu_link_save($unit_list);

		$unit_search = $unit_base;
		$unit_search['mlid'] = $mild;
		$unit_search['router_path'] = 'ticket/%/search';
		$unit_search['link_title'] = 'Search';
		$unit_search['link_path'] = 'ticket/'.$u_id.'/search';
		$unit_search['weight'] = 9;
		menu_link_save($unit_search);
		
		$unit_export = $unit_base;
		$unit_export['mlid'] = $mild;
		$unit_export['router_path'] = 'ticket/%/export';
		$unit_export['link_title'] = 'Export';
		$unit_export['link_path'] = 'ticket/'.$u_id.'/export';
		$unit_export['weight'] = 10;
		menu_link_save($unit_export);
	}
}

/**
 * ticket_admin_edit_unit: unit szerkesztes
 * @param $form_state - drupal $form_state variable
 * @param $u_id - unit id
 * @return array - $form: unit edit form
 */
function ticket_admin_unit_edit(&$form_state, $u_id) {
	/** breadcrumb kiegeszitese */
	$bc = drupal_get_breadcrumb();
	$bc[] = l(t('Units'), 'admin/ticket/unit/');
	drupal_set_breadcrumb($bc);

	/** unit lekerdezese, a vissza adott ertekek atadasa a formnak */
	$result = db_query("SELECT * FROM {ticket_unit} WHERE u_id = %d", $u_id);
	$def = db_fetch_array($result);
	
	$form = _ticket_admin_unit_form($def);
	$form['u_id'] = array(
		'#type' => 'value',
		'#value' => $u_id,
	);
	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save')
	);
	return $form;
}

/**
 * ticket_admin_edit_unit_validate: edit unit formrol erkezo adatok ellenorzese
 */
function ticket_admin_unit_edit_validate($form, &$form_state) {
	/** azonos nevu unit nem johet letre */
	$result = db_query("SELECT count(u_id) as num FROM {ticket_unit} WHERE u_id != %d AND unit_name = '%s'", $form_state['values']['u_id'], $form_state['values']['name']);
	$row = db_fetch_array($result);

	if ( $row['num'] ) {
		form_set_error('', t('You must select a name for this unit of settings.'));
	}
}

/**
 * ticket_admin_edit_unit_submit: unit modositas
 */
function ticket_admin_unit_edit_submit($form, &$form_state) {
	/** unit szerkesztese */
	$SQL = "UPDATE {ticket_unit} SET unit_name = '%s', description = '%s', weight = %d, active = %d WHERE u_id = %d";
	db_query($SQL, $form_state['values']['name'], $form_state['values']['desc'], $form_state['values']['weight'], $form_state['values']['active'], $form_state['values']['u_id']);
	
	/** unit menupont modositasa a ticket menu alatt */
	$menu = db_fetch_array(db_query("SELECT * FROM {menu_links} WHERE link_path = '%s'", 'ticket/'.$form_state['values']['u_id']));
	$menu['link_title'] = $form_state['values']['name'];
	$menu['options'] = array();
	menu_link_save($menu);
	drupal_goto('admin/ticket/unit');
}

/**
 * ticket_admin_unit_active: unit allapotanak modositasa: bekapcsolt / kikapcsolt
 */
function ticket_admin_unit_active($u_id, $active) {
	$SQL = "UPDATE {ticket_unit} SET active = %d WHERE u_id = %d";
	db_query($SQL, $active, $u_id);
	
	/** menupont ki/be kapcsolasa unit allapotanak megfeleloen */
	$menu = db_fetch_array(db_query("SELECT * FROM {menu_links} WHERE link_path = '%s'", 'ticket/'.$u_id));
	$menu['hidden'] = !$active;
	$menu['options'] = array();
	menu_link_save($menu);
	drupal_goto('admin/ticket/unit');
}




