<?php
/*
 * Created on 2006.08.01.
 *
 * PHP4 only!!!
 */

if (!function_exists('array_intersect_key')) {
	function array_intersect_key() {
		$arrs = func_get_args();
		$result = array_shift($arrs);
		foreach ($arrs as $array) {
			foreach ($result as $key => $v) {
				if (!array_key_exists($key, $array)) {
					unset($result[$key]);
				}
			}
		}
		return $result;
	}
}

if (!function_exists('str_split')) {
	function str_split($string,$split_length=1) {
		$count = strlen($string);
		if($split_length < 1) {
			return false;
		} elseif($split_length > $count) {
			return array($string);
		} else {
			$num = (int)ceil($count/$split_length);
			$ret = array();
			for($i=0;$i<$num;$i++) {
				$ret[] = substr($string,$i*$split_length,$split_length);
			}
			return $ret;
		}     
	} 
}