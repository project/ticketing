<?php
// Add table javascript.
?>

<table id="units" class="sticky-enabled">
	<thead>
		<tr>
			<th><?php echo t('Unit'); ?></th>
			<th><?php echo t('Weight'); ?></th>
			<th colspan="2"><?php echo t('Operations'); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php
		$row = 0;
		foreach (element_children($form) as $i):
			if ( !empty($form[$i]['info']) ):
		?>
		<tr class="draggable <?php echo $row % 2 == 0 ? 'odd' : 'even'; ?>">
			<td class="unit"><?php echo drupal_render($form[$i]['info']) ?></td>
			<td><?php echo drupal_render($form[$i]['weight']); ?></td>
			<td><?php echo drupal_render($form[$i]['configure']) ?></td>
			<td><?php echo drupal_render($form[$i]['active']) ?></td>
		</tr>
		<?php
			endif;
			$row++;
		endforeach; ?>
	</tbody>
</table>

<?php echo drupal_render($form); ?>