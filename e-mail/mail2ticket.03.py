#! /usr/bin/env python
# SQLAlchemy v0.3.x

# cat mail-ticket.txt | ./ticket.py ticket@york.georgikon.hu

import string, sys, time
import re, email, smtplib
import email.Header
from sqlalchemy import *

cfg = {}
""" SQL szerver beallitasai, egyezzen meg a drupal beallitasaival """
cfg['sql'] = {}
cfg['sql']['type'] = 'mysql'
cfg['sql']['server'] = 'localhost'
cfg['sql']['user'] = 'd6'
cfg['sql']['passwd'] = 'd6'
cfg['sql']['db'] = 'd6'
cfg['sql']['prefix'] = ''
cfg['mail'] = {}
cfg['mail']['server'] = 'localhost'
cfg['mail']['to'] = 'root'

def_charset = 'iso-8859-2'
""" drupal prefix hasznalata """
def db_prefix(table):
    return cfg['sql']['prefix']+table

try:
    """ kapcsolodas az SQL szerverhez """
    db = create_engine(cfg['sql']['type']+'://'+cfg['sql']['user']+':'+cfg['sql']['passwd']+'@'+cfg['sql']['server']+'/'+cfg['sql']['db'], echo=False, convert_unicode=False)
    conn = db.connect()
    
    """ E-Mail decodolasa """
    def m2decode(msg, charset):
        global def_charset
    
        if charset != None:
            try:
                txt = msg.get_payload(decode=True).decode(charset)
            except:
                try:
                    txt = msg.get_payload(decode=True).decode(def_charset)
                except:
                    txt = msg.get_payload(decode=True).decode('utf8', 'replace')
        else:
            try:
                txt = msg.get_payload(decode=True).decode(def_charset)
            except:
                try:
                    txt = msg.get_payload(decode=True).decode('utf8', 'replace')
                except:
                    return None
    
        return txt
    
    """ Az adtatbazis strukturak es osztalyok """
    queue_T = Table(cfg['sql']['prefix']+'ticket_queue', db,
        Column('qid', Integer, primary_key=True),
        Column('uid', Integer),
        Column('gid', Integer),
        Column('encode', Integer),
        Column('priority_id', Integer),
        Column('scope_id', Integer),
        Column('site_id', Integer),
        Column('created', Integer),
        Column('summary', String(255)),
        Column('detail', TEXT)
    )
    
    class db_Queue(object):
        def __repr__(self):
            return "(qid %s, uid %s, gid %s, priority_id %s, scope_id %s, site_id %s, created %s, summary %s, detail %s)" % (self.qid, self.uid, self.gid, self.priority_id, self.scope_id, self.site_id, self.created, self.summary, self.detail)
    
    ticket_T = Table(cfg['sql']['prefix']+'ticket_ticket', db,
        Column('tid', Integer, primary_key=True),
        Column('uid', Integer),
        Column('gid', Integer),
        Column('encode', Integer),
        Column('priority_id', Integer),
        Column('type_id', Integer),
        Column('scope_id', Integer),
        Column('site_id', Integer),
        Column('fixer_id', Integer),
        Column('assigned', Integer),
        Column('summary', String(255)),
        Column('created', Integer),
        Column('start', Integer),
        Column('detail', TEXT),
        Column('affected', TEXT),
        Column('actions', TEXT),
        Column('fix', TEXT)
    )
    
    """ Hasznalt tablazatok konfiguralasa """
    class db_Ticket(object):
        def __repr__(self):
            return "(tid %s, uid %s, gid %s, priority_id %s, type_id %s, scope_id %s, site_id %s, fixer_id %s, assigned %s, summary %s, created %s, start %s, detail %s, affected %s, actions %s, fix %s)" % (self.tid, self.uid, self.gid, self.priority_id, self.type_id, self.scope_id, self.site_id, self.fixer_id, self.assigned, self.summary, self.created, self.start, self.detail, self.affected, self.actions, self.fix)
    
    mail_T = Table(cfg['sql']['prefix']+'ticket_email', db,
        Column('email_id', Integer, primary_key=True),
        Column('gid', Integer),
        Column('def', Integer, key='default'),
        Column('active', Integer)
    )
    
    class db_Mail(object):
        def __repr__(self):
            return "(email_id %s, gid %s, default %s, active %s)" % (self.email_id, self.gid, self.default, self.active)
    
    event_T = Table(cfg['sql']['prefix']+'ticket_event', db,
        Column('eid', Integer, primary_key=True),
        Column('tid', Integer),
        Column('uid', Integer),
        Column('reassigned', Integer),
        Column('created', Integer),
        Column('description', TEXT),
        Column('status_id', Integer)
    )
    
    class db_Event(object):
        def __repr__(self):
            return "(eid %s, tid %s, uid %s, reassigned %s, created %s, description %s, status_id %s)" % (self.eid, self.tid, self.uid, self.reassigned, self.created, self.description, self.status_id)
    
    rel_event_T = Table(cfg['sql']['prefix']+'ticket_rel_event', db,
        Column('tid', Integer, primary_key=True),
        Column('eid', Integer, primary_key=True),
        Column('num', Integer),
    )
    
    class db_rel_Event(object):
        def __repr__(self):
            return "(tid %s, eid %s, num %s)" % (self.tid, self.eid, self.num)
    
    fixer_T = Table(cfg['sql']['prefix']+'ticket_fixer', db,
        Column('fixer_id', Integer, primary_key=True),
        Column('gid', Integer),
        Column('fixer_name', String(255)),
        Column('def', Integer, key='default'),
        Column('active', Integer)
    )
    
    class db_Fixer(object):
        def __repr__(self):
            return "(fixer_id %s, gid %s, fixer_name %s, default %s, active %s)" % (self.fixer_id, self.gid, self.fixer_name, self.default, self.active)
    
    priority_T = Table(cfg['sql']['prefix']+'ticket_priority', db,
        Column('priority_id', Integer, primary_key=True),
        Column('gid', Integer),
        Column('priority_name', String(255)),
        Column('def', Integer, key='default'),
        Column('active', Integer)
    )
    
    class db_Priority(object):
        def __repr__(self):
            return "(priority_id %s, gid %s, priority_name %s, default %s, active %s)" % (self.priority_id, self.gid, self.priority_name, self.default, self.active)
    
    rel_mail_T = Table(cfg['sql']['prefix']+'ticket_rel_ticket_email', db,
        Column('tid', Integer, primary_key=True),
        Column('email_id', Integer, primary_key=True),
    )
    
    class db_rel_Mail(object):
        def __repr__(self):
            return "(tid %s, email_id %s)" % (self.tid, self.email_id)
    
    scope_T = Table(cfg['sql']['prefix']+'ticket_scope', db,
        Column('scope_id', Integer, primary_key=True),
        Column('gid', Integer),
        Column('scope_name', String(255)),
        Column('def', Integer, key='default'),
        Column('active', Integer)
    )
    
    class db_Scope(object):
        def __repr__(self):
            return "(scope_id %s, gid %s, scope_name %s, default %s, active %s)" % (self.scope_id, self.gid, self.scope_name, self.default, self.active)
    
    site_T = Table(cfg['sql']['prefix']+'ticket_site', db,
        Column('site_id', Integer, primary_key=True),
        Column('gid', Integer),
        Column('site_name', String(255)),
        Column('def', Integer, key='default'),
        Column('active', Integer)
    )
    
    class db_Site(object):
        def __repr__(self):
            return "(site_id %s, gid %s, site_name %s, default %s, active %s)" % (self.site_id, self.gid, self.site_name, self.default, self.active)
    
    status_T = Table(cfg['sql']['prefix']+'ticket_status', db,
        Column('status_id', Integer, primary_key=True),
        Column('gid', Integer),
        Column('status_name', String(255)),
        Column('closed', Integer),
        Column('def', Integer, key='default'),
        Column('active', Integer),
    )
    
    class db_Status(object):
        def __repr__(self):
            return "(status_id %s, gid %s, status_name %s, closed %s, default %s, active %s)" % (self.site_id, self.gid, self.site_name, self.closed, self.default, self.active)
    
    type_T = Table(cfg['sql']['prefix']+'ticket_type', db,
        Column('type_id', Integer, primary_key=True),
        Column('gid', Integer),
        Column('type_name', String(255)),
        Column('def', Integer, key='default'),
        Column('active', Integer)
    )
    
    class db_Type(object):
        def __repr__(self):
            return "(type_id %s, gid %s, type_name %s, default %s, active %s)" % (self.type_id, self.gid, self.type_name, self.default, self.active)
    
    template_T = Table(cfg['sql']['prefix']+'ticket_template', db,
        Column('gid', Integer, primary_key=True),
        Column('mail_address', String(255)),
        Column('sender_address', String(255)),
    )
    
    class db_Template(object):
        def __repr__(self):
            return "(gid %s, mail_address %s, sender_address %s)" % (self.gid , self.mail_address , self.sender_address)
    
    """ Szukseges mapperek elkeszitese """
    queuemapper = mapper(db_Queue, queue_T)
    ticketmapper = mapper(db_Ticket, ticket_T)
    mailmapper = mapper(db_Mail, mail_T)
    eventmapper = mapper(db_Event, event_T)
    rel_eventmapper = mapper(db_rel_Event, rel_event_T)
    fixermapper = mapper(db_Fixer, fixer_T)
    prioritymapper = mapper(db_Priority, priority_T)
    rel_mailmapper = mapper(db_rel_Mail, rel_mail_T)
    scopemapper = mapper(db_Scope, scope_T)
    sitemapper = mapper(db_Site, site_T)
    statusmapper = mapper(db_Status, status_T)
    typemapper = mapper(db_Type, type_T)
    templatemapper = mapper(db_Template, template_T)

except: 
    msg = email.message_from_file(sys.stdin)
    s = smtplib.SMTP()
    s.connect()
    s.sendmail(msg.get('From'), cfg['mail']['to'], msg.as_string())
    s.close()
    sys.exit()

    #print dir(msg)
    #print msg.as_string()
    # Send the email via our own SMTP server.

if __name__ == '__main__':
    """ level beolvasasa stdin-rol """
    msg = email.message_from_file(sys.stdin)
    try:
        """ Levelnek van feladoja es van parametere a scriptnek """
        if (msg.get('From') != None) and (len(sys.argv) > 1):
            """ Felado e-mail cimenek kinyerese """
            sender = email.Utils.parseaddr(msg.get('From'))

            session = create_session()
            """ A megfelelo mail template kikeresese """
            query = session.query(db_Template)
            template = query.get_by(mail_address = sys.argv[1])
            if template != None:
                """ A lehetseges feladok listajanak elkeszitese """
                sender_address = template.sender_address
                sender_address = sender_address.replace(', ', ' ')
                sender_address = sender_address.replace(',', ' ')
                sender_address = sender_address.split(' ')
                """ Ha ticket 0: akkor a mail a ticket_queue tablaba kerul """
                ticket = 0
                for addr in sender_address:
                    if addr:
                        """ '.' karakter escapelese, valamint a * joker karakter regexp jokerre alakitasa """
                        addr = addr.replace('.', '\.')
                        addr = addr.replace('*', '.*')
                        """ ha van egyezes akkor a mail a ticket_ticket tablaba kerul """
                        if re.search(addr, sender[1]):
                            ticket = 1
                gid = template.gid

                """ Ticket vagy Queue tablaba kerules elokeszitese """
                if ticket:
                    t_query = session.query(db_Ticket)
                    new_ticket = db_Ticket()
                else:
                    t_query = session.query(db_Queue)
                    new_ticket = db_Queue()

                """ Default ertekekkel valo feltoltes ami azonos a Queue es a Ticket eseteben (priority, scope, site) """
                p_id_query = session.query(db_Priority)
                p_id = p_id_query.get_by(gid = gid, active = 1, default = 1)
                sc_id_query = session.query(db_Scope)
                sc_id = sc_id_query.get_by(gid = gid, active = 1, default = 1)

                si_id_query = session.query(db_Site)
                si_id = si_id_query.get_by(gid = gid, active = 1, default = 1)
                
                new_ticket.gid = gid
                new_ticket.uid = 0
                new_ticket.priority_id = p_id.priority_id
                
                if sc_id:
                    new_ticket.scope_id = sc_id.scope_id
                else:
                    new_ticket.scope_id = 1

                if si_id:
                    new_ticket.site_id = si_id.site_id
                else:
                    new_ticket.site_id = 1

                new_ticket.encode = 0

                """ UNIX timestamp keszitese e-mail feladasi datumabol """
                date = msg.get('Date')
                created = int(email.Utils.mktime_tz(email.Utils.parsedate_tz(date)))
                
                """ Szoveg atkodolasa UTF-8-ra """
                summary = unicode(email.Header.make_header(email.Header.decode_header(msg.get('subject'))))
                if summary != summary.encode('ascii', 'xmlcharrefreplace'):
                    new_ticket.encode = 1
                new_ticket.summary = summary.encode('ascii', 'xmlcharrefreplace')

                detail = ''
                if msg.is_multipart():
                    for part in msg.walk():
                        charset = part.get_charsets()
                        txt = m2decode(part, charset[0])
                        if txt != None:
                            if detail != '':
                                detail += "\n"
                            detail += txt
                else:
                    charset = msg.get_charsets()
                    txt = m2decode(msg, charset[0])
                    if txt != None:
                        detail += txt

                if detail != detail.encode('ascii', 'xmlcharrefreplace'):
                    new_ticket.encode = 1

                new_ticket.detail = detail.encode('ascii', 'xmlcharrefreplace')
                """ Ticket vagy Queue hozza adasa """
                if ticket:
                    """ Tickethez szukseges ertekek feltoltese (assigned, type, fixer) """
                    new_ticket.assigned = 0

                    tt_id_query = session.query(db_Type)
                    tt_id = tt_id_query.get_by(gid = gid, active = 1, default = 1)
                    new_ticket.type_id = tt_id.type_id

                    f_id_query = session.query(db_Fixer)
                    f_id = f_id_query.get_by(gid = gid, active = 1, default = 1)
                    if f_id:
                        new_ticket.fixer_id = f_id.fixer_id
                    else:
                        new_ticket.fixer_id = 1

                    """ Aktualis datum lekerdezese """
                    now = int(time.mktime(time.localtime()))
                    new_ticket.created = now
                    new_ticket.start = created
		    
                    """ Mezoket ures ertekkel feltolteni"""
                    new_ticket.affected = new_ticket.actions = new_ticket.fix = ''

                    """ Ticket elmentese, SQL utasitasok vegrehajtasa """
                    session.save(new_ticket)
                    session.flush()

                    """ Ures event letrehozasa """
                    tid = new_ticket.tid
                    new_event = db_Event()
                    new_event.tid = tid
                    new_event.uid = 0
                    new_event.reassigned = 0
                    new_event.created = now
                    new_event.description = ''

                    """ Eventhez szukseges alapertelmezett status lekerdezese """
                    status_id_query = session.query(db_Status)
                    status_id = status_id_query.get_by(gid = gid, active = 1, default = 1, closed = 0)
                    new_event.status_id = status_id.status_id
                    """ event elmentese """
                    session.save(new_event)
                    session.flush()

                    eid = new_event.eid

                    new_rel_event = db_rel_Event()
                    new_rel_event.tid = tid
                    new_rel_event.eid = eid
                    new_rel_event.num = 0
                    session.save(new_rel_event)

                    """ Ticket - mail osszekapcsolasa """
                    m_id_query = session.query(db_Mail)
                    m_id = m_id_query.select_by(gid = gid, active = 1, default = 1)
                    if m_id:
                        for mid in m_id:
                            new_rel_mail = db_rel_Mail()
                            new_rel_mail.tid = tid
                            new_rel_mail.email_id = mid.email_id
                            session.save(new_rel_mail)

                else:
                    """ Queue-hoz szukseges egyedi tulajdonsagok beallitasa """
                    new_ticket.created = created
                    """ Queue elmentese, SQL utasitasok vegrehajtasa """
                    session.save(new_ticket)

                session.flush()
                """ Session lezarasa """
                session.close()

    except: 
        s = smtplib.SMTP()
        s.connect()
        s.sendmail(msg.get('From'), cfg['mail']['to'], msg.as_string())
        s.close()
        sys.exit()

""" Kapcsolat lezarasa """
conn.close()
