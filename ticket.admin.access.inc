<?php

/**
 * _ticket_system_admin_menu_set: admin jogosultsaghoz eleretove teszi az admin menut az aktualis roleban
 * @param $rid  drupal role id
 */
function _ticket_system_admin_menu_set($rid) {
	$perm = '';
	$SQL = "SELECT p.perm FROM {permission} p, {ticket_permission} tp"; 
	$SQL .= " WHERE tp.rid = p.rid AND p.rid = %d AND tp.perm LIKE '%ticket administer%'";
	$result = db_query($SQL, $rid);
	if ( $row = db_fetch_array($result) ) {
		if ( strpos($row['perm'], 'access administration pages') === FALSE ) {
			$perms = explode(', ', $row['perm']);
			$perms[] = 'access administration pages';
			$perm = implode(', ', $perms);
		}
	} else {
		$SQL = "SELECT p.perm FROM {permission} p WHERE p.rid = %d";
		$result = db_query($SQL, $rid);
		if ( $row = db_fetch_array($result) ) {
			if ( !(strpos($row['perm'], 'access administration pages') === FALSE) ) {
				$perms = explode(', ', $row['perm']);
				if ( !( array_search('access administration pages', $perms) === FALSE) ) {
					$key = array_search('access administration pages', $perms);
					unset($perms[$key]);
					$perm = implode(', ', $perms);
				}
			}
		}
	}
	if ($perm != '') {
		$SQL = "UPDATE {permission} SET perm = '%s' WHERE rid = %d";
		db_query($SQL, $perm, $rid);
	}
}

/**
 * ticket_admin_access: jogosultsag kezeles
 */
function ticket_admin_access() {
	$SQL = "SELECT r.rid, r.name FROM {role} r, {permission} p"; 
	$SQL .= " WHERE r.rid = p.rid AND p.perm LIKE '%ticket access%' ORDER BY r.name";
	$result = db_query($SQL);
	$roles = array();
	while ($row = db_fetch_array($result)) {
		if ( ticket_role_admin_access($row['rid']) ) {
			$rows[] = array ($row['name'], l(t('edit'), 'admin/ticket/access/edit/'.$row['rid']), l(t('delete'), 'admin/ticket/access/delete/'.$row['rid']));
		} else {
			$rows[] = array ($row['name'], '', '');
		}
	}
	return theme_table(array(t('Permission'), t('Operations')), $rows);
}

/**
 * ticket_admin_access_edit_list_form: Ticket access scope admisztracios listaja
 * @param $perms  form settings
 * @return array - $form: scope list form
 */
function ticket_admin_access_edit_list_form(&$form_state, $var) {
	$rid = $var['rid'];
	$perms = $var['perms'];
	unset($var);
		
	$form['#tree'] = TRUE;
	foreach ($perms as $key => $perm) {
		$form[$key]['name'] = array( '#value' => $perm['name'] );
		foreach ($perm['access'] as $acc_key => $access) {
			$form[$key][$acc_key] = array(
				'#type' => 'checkbox',
				'#default_value' => $access,
			);
		}
		$form[$key]['delete'] = array('#value' => l(t('delete'), 'admin/ticket/access/edit/'.$rid.'/delete/'.$key) );
	}
	
	$result = db_query("SELECT uid, name FROM {users} WHERE status = 1 ORDER BY name");
	while ( $row = db_fetch_array($result) ) {
		$users[$row['uid']] = $row['name'];
	}
	unset($users[0]);

	$SQL = "SELECT uid FROM {users_roles} WHERE rid = %d";
	$result = db_query($SQL, $rid);
	while ( $row = db_fetch_array($result) ) {
		$set_users[] = $row['uid'];
	}
	
	$form['users'] = array(
		'#type' => 'checkboxes',
		'#title' => t('Users'),
		'#options' => $users,
		'#default_value' => isset($set_users) ? $set_users : array(),
		'#prefix' => '<div class="ticket-chb-scroll">',
		'#suffix' => '</div>',
	);

	$form['rid'] = array(
		'#type' => 'value',
		'#value' => $rid,
	);
	
	$form["submit"] = array(
		"#type" => "submit",
		"#value" => t('Save configuration'),
	);

	return $form;
}

/**
 * ticket_admin_access_edit_list_form_validate: Ticket access scope listan vegzet modositasok ellenorzese
 */
function ticket_admin_access_edit_list_form_validate($form, &$form_state) {
	foreach ($form_state['values'] as $role => $access) {
		if ( strpos($role, '-') ) {
			if ( !(array_sum($access)) ) {
				form_set_error('', t('At least one access-credential must be chosen.'));
			}
		}
	}
}

/**
 * ticket_admin_access_edit_list_form_submit: Ticket access scope listan vegzet modositasok mentese
 */
function ticket_admin_access_edit_list_form_submit($form, &$form_state) {
	foreach ($form_state['values'] as $role => $access) {
		if ( strpos($role, '-') ) {
			$perms = array();
			list($u_id, $gid) = explode('-', $role);
			foreach ($access as $key => $var) {
				if ($var) {
					$perms[] = $key;
				}
			}
			$SQL = "UPDATE {ticket_permission} SET perm = '%s' WHERE rid = %d AND u_id = %d AND gid = %d";
			db_query($SQL, implode(', ', $perms), $form_state['values']['rid'], $u_id, $gid);
		}
	}
	// felhasznalok csoporthoz valo hozza rendelese
	if ($form_state['values']['rid'] > 2) {
		$form_state['values']['users'] = array_diff($form_state['values']['users'], array(0));
		db_query('DELETE FROM {users_roles} WHERE rid = %d', $form_state['values']['rid']);
		if ( count($form_state['values']['users']) ) {
			foreach ($form_state['values']['users'] as $uid) {
				db_query('INSERT INTO {users_roles} (uid, rid) VALUES (%d, %d)', $uid, $form_state['values']['rid']);
			}
		}
	}
	
	_ticket_system_admin_menu_set($form_state['values']['rid']);
}

/**
 * ticket_admin_access_scope_add_form: jogosultsaghoz hatokor hozza adasa
 * @param $form_state - drupal form variable
 * @param $perms  form settings
 * @return array $form  drupal form
 */
function ticket_admin_access_scope_add_form(&$form_state, $var) {
	global $user;
	
	$rid = $var['rid'];
	$perms = $var['perms'];
	unset($var);
	$admin_access = _ticket_role_admin_list();
	
	$scope = array();
	if (!isset($perms['0-0']) ) {
		if ( ($user->uid == 1) || ($admin_access[0][0]) ) {
			$scope['0-0'] = t('All');
		}
	}
	
	if ( empty($user->ticket_perm) ) {
		ticket_access('ticket administer', 0, 0);
	}
	$result = db_query("SELECT u_id, unit_name FROM {ticket_unit} WHERE active = 1 ORDER BY unit_name");
	while ($row = db_fetch_array($result)) {
		if (!isset($perms[$row['u_id'].'-0']) ) {
			if ( _ticket_access($user->ticket_perm, $row['u_id'], 0, array('ticket administer') ) ) {
				$scope[$row['u_id'].'-0'] = $row['unit_name'];
			}
		}
		$result_g = db_query("SELECT u_id, gid, group_name FROM {ticket_group} WHERE active = 1 AND u_id = %d ORDER BY group_name", $row['u_id']);
		while ($row_g = db_fetch_array($result_g)) {
			if (!isset($perms[$row['u_id'].'-'.$row_g['gid']]) ) {
				if ( _ticket_access($user->ticket_perm, $row['u_id'], $row_g['gid'], array('ticket administer') ) ) {
					$scope[$row['u_id'].'-'.$row_g['gid']] = $row['unit_name'].' / '.$row_g['group_name'];
				}
			}
		}
	}
	if ( count($scope) ) {
		$form['add'] = array(
			'#type' => 'fieldset',
			'#title' => t('Add scope').':',
			'#collapsible' => TRUE,
			'#collapsed' => false,
		);
	
		$form['add']['scope'] = array(
			'#type' => 'select',
			'#title' => t('Scope'),
			'#options' => $scope,
		);
		
		$ticket_access = ticket_perms();
		foreach ($ticket_access as $ta) {
			$ta_list[$ta] = t($ta);
		}
	
		$form['add']['perm'] = array(
			'#type' => 'checkboxes',
			'#title' => t('Access'),
			'#options' => $ta_list,
		);
		
		$form['rid'] = array(
			'#type' => 'value',
			'#value' => $rid,
		);
		
		$form['add']['submit'] = array(
			'#type' => 'submit',
			'#value' => t('Save')
		);
	}

	return $form;
}

/**
 * ticket_admin_access_scope_add_form_validate: hatokor jogosultsaghoz valo hozzarendeles ellenorzese 
 */
function ticket_admin_access_scope_add_form_validate($form, &$form_state) {
	$form_state['values']['perm'] = array_diff($form_state['values']['perm'], array(0));
	if ( !count($form_state['values']['perm']) ) {
		form_set_error('perm', t('At least one access-credential must be chosen.'));
	}
}

/**
 * ticket_admin_access_scope_add_form_submit: hatokor jogosultsaghoz valo hozzarendelese 
 */
function ticket_admin_access_scope_add_form_submit($form, &$form_state) {
	$form_state['values']['perm'] = array_diff($form_state['values']['perm'], array(0));
	$SQL = "INSERT INTO {ticket_permission} (rid, u_id, gid, perm) VALUES (%d, %d, %d, '%s')";
	list($u_id, $gid) = explode('-', $form_state['values']['scope']);
	db_query($SQL, $form_state['values']['rid'], $u_id, $gid, implode(', ', $form_state['values']['perm']));
	_ticket_system_admin_menu_set($form_state['values']['rid']);
}


/**
 * ticket_admin_access_edit: jogosultsag hatokor beallitasa
 * @param $rid  role id
 */
function ticket_admin_access_edit($rid) {
	global $user;
	
	$access = ticket_perms();
	
	$name['u_id'][0] = '';
	$name['gid'][0] = '';
	$result = db_query("SELECT u_id, gid, perm FROM {ticket_permission} WHERE rid = %d", $rid);
	while ($row = db_fetch_array($result)) {
		$r_access = explode(', ', $row['perm']);
		$r_access = array_flip($r_access);
		foreach ($access as $acc) {
			if ( isset($r_access[$acc]) ) {
				$r_access[$acc] = 1;
			} else {
				$r_access[$acc] = 0;
			}
		}
		
		$perms[$row['u_id']][$row['gid']]['access']= $r_access;
		if ( !isset($name['u_id'][$row['u_id']]) ) {
			$rs = db_query("SELECT unit_name FROM {ticket_unit} WHERE u_id = %d", $row['u_id']);
			$r = db_fetch_array($rs);
			$name['u_id'][$row['u_id']] = $r['unit_name'];
		}
		
		if ( !isset($name['gid'][$row['gid']]) ) {
			$rs = db_query("SELECT group_name FROM {ticket_group} WHERE gid = %d", $row['gid']);
			$r = db_fetch_array($rs);
			$name['gid'][$row['gid']] = $r['group_name'];
		}
		if ( $row['gid'] ) {
			$acc_name = $name['u_id'][$row['u_id']].' / '.$name['gid'][$row['gid']];
		} else {
			$acc_name = $name['u_id'][$row['u_id']];
		}
		
		$perms[$row['u_id']][$row['gid']]['name']= $acc_name;
	}
	
	asort($name['u_id']);
	asort($name['gid']);
	if ( isset($perms[0][0]['name']) ) {
		$perms[0][0]['name']= t('All');
	}
	foreach ($name['u_id'] as $ukey => $unit) {
		foreach ($name['gid'] as $gkey => $group) {
			if ( isset($perms[$ukey][$gkey]) ) {
				$var['perms'][$ukey.'-'.$gkey] = $perms[$ukey][$gkey];
			}
		}
	}
	$var['rid'] = $rid;
	unset($perms);
	if ( count($var['perms']) ) {
		$output .= drupal_get_form('ticket_admin_access_edit_list_form', $var);
	}
	$output .= drupal_get_form('ticket_admin_access_scope_add_form', $var);
	
	$result = db_query("SELECT name FROM {role} WHERE rid = %d", $rid);
	$row = db_fetch_array($result);

	drupal_set_title( t('Edit access: !role_name', array('!role_name' => $row['name'])) );
	/** breadcrumb kiegeszitese */
	$bc = drupal_get_breadcrumb();
	$bc[] = l(t('Access ticketing'), 'admin/ticket/access');
	drupal_set_breadcrumb($bc);
	
	return $output;
}

/**
 * ticket_admin_access_scope_delete: hatokor torlese a jogosultsagbol
 * @param $rid  role id
 * @param $perm  permission
 */
function ticket_admin_access_scope_delete($rid, $perm) {
	list($u_id, $gid) = explode('-', $perm);
	if ( ticket_access('ticket administer', $u_id, $gid) ) {
		$SQL = "DELETE FROM {ticket_permission} WHERE rid = %d AND u_id = %d AND gid = %d";
		db_query($SQL, $rid, $u_id, $gid);
	}
	_ticket_system_admin_menu_set($rid);
	drupal_goto('admin/ticket/access/edit/'.$rid);
}

/**
 * ticket_admin_access_add: jogosultsag letrehozasa
 */
function ticket_admin_access_add() {
	$form['name'] = array(
		'#type' => 'textfield',
		'#title' => t('Name'),
		'#description' => t("All credentials begin with prefix \"tk_\"."),
		'#size' => 60,
		'#maxlength' => 60,
		'#required' => true,
	);

	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save')
	);

	return $form;
}

/**
 * ticket_admin_access_add_validate: jogosultsag letrehozasanak ellenorzese
 */
function ticket_admin_access_add_validate($form, &$form_state) {
	$result = db_query("SELECT * FROM {role} WHERE name = '%s'", 'tk_'.$form_state['values']['name']);
	if ( $row = db_fetch_array($result) ) {
		form_set_error('name', t('Already-existing creidentials!'));
	}
}

/**
 * ticket_admin_access_add_submit: jogosultsag letrehozasa
 */
function ticket_admin_access_add_submit($form, &$form_state) {
	db_query("INSERT INTO {role} (name) VALUES ('%s')", 'tk_'.$form_state['values']['name']);
	$rid = db_last_insert_id('{role}', 'rid');
	db_query("INSERT INTO {permission} (rid, perm) VALUES (%d, 'ticket access')", $rid);
}

/**
 * ticket_admin_access_delete_confirm: access torles megerosites
 */
function ticket_admin_access_delete_confirm(&$form_state, $rid) {
	// megerosites
	$result = db_query("SELECT name FROM {role} WHERE rid = %d", $rid);
	if ($row = db_fetch_array($result)) {
		$form['rid'] = array('#type' => 'value', '#value' => $rid);
	    return confirm_form($form,
			t('Are you sure you want to delete !title?', array('!title' => theme('placeholder', $row['name']))),
			'admin/ticket/access', t('This action cannot be undone.'),
			t('Delete'), t('Cancel')  );
	} else {
		drupal_access_denied();
	}
}

/**
 * ticket_admin_access_delete_confirm_submit: access torles
 */
function ticket_admin_access_delete_confirm_submit($form, &$form_state) {

	// jogosultsag nevenek lekerdezese
	$result = db_query("SELECT name FROM {role} WHERE rid = %d", $form_state['values']['rid']);
	$row = db_fetch_array($result);
	if ( substr($row['name'], 0, 3) == 'tk_' ) {
		// a ticket jogosultsagokat torolni kell mindenhonnan
		db_query('DELETE FROM {role} WHERE rid=%d', $form_state['values']['rid']);
		db_query('DELETE FROM {permission} WHERE rid=%d', $form_state['values']['rid']);
		db_query('DELETE FROM {users_roles} WHERE rid=%d', $form_state['values']['rid']);
	} else {
		// nem csak ticketre hasznalt jogosultsag, specialisan kezelendo
		$admin = 0;
		$result = db_query("SELECT perm FROM {ticket_permission} WHERE rid = %d AND perm LIKE '%ticket administer%'", $form_state['values']['rid']);
		if ( $row = db_fetch_array($result) ) {
			$admin = 1;
		}
		$result = db_query("SELECT perm FROM {permission} WHERE rid = %d", $form_state['values']['rid']);
		$row = db_fetch_array($result);
		$perm = explode(', ', $row['perm']);
		if ( $admin ) {
			$key = array_search('access administration pages', $perm);
			unset($perm[$key]);
		}
		$key = array_search('ticket access', $perm);
		unset($perm[$key]);
		$perm = implode(', ', $perm);
		db_query("UPDATE {permission} SET perm = '%s' WHERE rid = %d", $perm, $form_state['values']['rid']);
	}
	db_query('DELETE FROM {ticket_permission} WHERE rid=%d', $form_state['values']['rid']);
	drupal_goto('admin/ticket/access');
}

/**
 * ticket_admin_access_users: ticket jogosultsag hoza rendelese felhasznalohoz.
 */
function ticket_admin_access_users() {
	drupal_add_js(drupal_get_path('module','ticket').'/ticket.js');
	drupal_add_css(drupal_get_path('module', 'ticket').'/ticket.css');

	// felhasznalok listaja
	$users[0] = t('Choose one please!');
	$result = db_query('SELECT uid, name FROM {users} WHERE status = 1');
	while ($row = db_fetch_array($result)) {
		$users[$row['uid']] = $row['name'];
	}
	
	$roles = array();
	// azoknak a jogosultsagoknak a lekerdezese amiket adminisztralhat
	$SQL = "SELECT r.rid, r.name FROM {role} r, {permission} p";
	$SQL .= " WHERE r.rid = p.rid AND p.perm LIKE '%ticket access%' ORDER BY r.name";
	$result = db_query($SQL);
	while ($row = db_fetch_array($result)) {
		if ( (ticket_role_admin_access($row['rid'])) && ($row['rid'] > 2) ) {
			$roles[$row['rid']] = $row['name'];
		}
	}

	if ( $uid ) {
		// a jogosultsagainak lekerdezese amiket az adminisztrator lathat.
		$SQL = "SELECT p.rid FROM {permission} p, {users_roles} ur";
		$SQL .= " WHERE p.rid = ur.rid AND ur.uid = %d AND p.perm LIKE '%ticket access%'";
		$result = db_query($SQL, $uid);
		while ($row = db_fetch_array($result)) {
			if ( $roles[$row['rid']] ) {
				$uroles[] = $row['rid'];
			}
		}
	}
	
	if ( count($roles) ) {
		$form['user'] = array(
			'#type' => 'select',
			'#title' => t('User'),
			'#options' => $users,
		);

		$form['perm'] = array(
			'#type' => 'fieldset',
			'#title' => t('Permission').':',
			'#collapsible' => TRUE,
			'#collapsed' => false,
		);
		$form['perm']['roles'] = array(
			'#type' => 'checkboxes',
			'#options' => $roles,
			'#default_value' => isset($uroles) ? $uroles : array(),
			'#prefix' => '<div class="ticket-chb-scroll">',
			'#suffix' => '</div>',
		);
		
		$form['submit'] = array(
			'#type' => 'submit',
			'#value' => t('Save'),
		);
	} else {
		$form = array();
	}
	
	return $form;
}

/**
 * ticket_admin_access_users_validate: ticket jogosultsag hoza rendeles ellenorzese
 */
function ticket_admin_access_users_validate($form, &$form_state) {
	if ( ! $form_state['values']['user'] ) {
		form_set_error('user', t('Choose a user please!'));
	}
}

/**
 * ticket_admin_access_users_submit: ticket jogosultsag hoza rendelese felhasznalohoz.
 */
function ticket_admin_access_users_submit($form, &$form_state) {
	// felhasznalo csoport listaja
	$roles = array();
	$result = db_query('SELECT rid FROM {users_roles} WHERE uid = %d', $form_state['values']['user']);
	while ($row = db_fetch_array($result)) {
		$roles[] = $row['rid'];
	}
	// ticket csoportok eltavolitasa a listabol
	$roles = array_diff($roles, array_keys($form_state['values']['roles']));
	// formrol erkezo csoportlistabol kiszurjuk a ki nem valasztott csoportokat
	$form_state['values']['roles'] = array_diff($form_state['values']['roles'], array(0));
	// az altalanos csoportok es a ticket csoportok osszefesulese
	$roles = array_merge($roles, $form_state['values']['roles']);
	
	// adatbazis muveletek elvegzese
	db_query('DELETE FROM {users_roles} WHERE uid = %d', $form_state['values']['user']);
	foreach ($roles as $role) {
		db_query('INSERT INTO {users_roles} (uid, rid) VALUES (%d, %d)', $form_state['values']['user'], $role);
	}
}

function ticket_admin_access_users_select($uid) {
	if ( is_numeric($uid) ) {
		// azoknak a jogosultsagoknak a lekerdezese amiket adminisztralhat
		$SQL = "SELECT r.rid, r.name FROM {role} r, {permission} p";
		$SQL .= " WHERE r.rid = p.rid AND p.perm LIKE '%ticket access%' ORDER BY r.name";
		$result = db_query($SQL);
		while ($row = db_fetch_array($result)) {
			if ( (ticket_role_admin_access($row['rid'])) && ($row['rid'] > 2) ) {
				$roles[$row['rid']] = 0;
			}
		}
		// a jogosultsagainak lekerdezese amiket az adminisztrator lathat.
		$SQL = "SELECT p.rid FROM {permission} p, {users_roles} ur";
		$SQL .= " WHERE p.rid = ur.rid AND ur.uid = %d AND p.perm LIKE '%ticket access%'";
		$result = db_query($SQL, $uid);
		while ($row = db_fetch_array($result)) {
			if ( isset($roles[$row['rid']]) ) {
				$roles[$row['rid']] = 1;
			}
		}
		
		echo drupal_to_js(isset($roles) ? $roles : array());
	}
	exit;
}

